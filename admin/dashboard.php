<?php  
		session_start();
	    $adminid=$_SESSION['adminid'];	  
	    $odoooid=$_SESSION['odooid'];	  
	    if(!is_numeric($adminid)){header("Location: index.php");}
	    
		include('include/header.php');
?>

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="container-fluid mt-3">
                <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <a href="payments.php">
						  <div class="card gradient-1">
                             <div class="card-body">
                                <h3 class="card-title text-white">Payments</h3>
                                <span class="float-right display-5 opacity-5"><i class="fa fa-money"></i></span>
                             </div>
                          </div>
						</a>
                    </div>
                    <div class="col-lg-3 col-sm-6">
					    <a href="reading.php">
							<div class="card gradient-2">
								<div class="card-body">
									<h3 class="card-title text-white">Readings</h3>
									<span class="float-right display-5 opacity-5"><i class="fa fa-tachometer"></i></span>
								</div>
							</div>
						</a>
                    </div>
					
					<div class="col-lg-3 col-sm-6">
					    <a href="bulk-sms.php">
							<div class="card gradient-3">
								<div class="card-body">
									<h3 class="card-title text-white">Bulk Sms</h3>
									<span class="float-right display-5 opacity-5"><i class="fa fa-mobile"></i></span>
								</div>
							</div>
						</a>
                    </div>
					
					<div class="col-lg-3 col-sm-6">
					    <a href="bulk-email.php">
							<div class="card gradient-8">
								<div class="card-body">
									<h3 class="card-title text-white">Bulk E-mail</h3>
									<span class="float-right display-5 opacity-5"><i class="fa fa-envelope"></i></span>
								</div>
							</div>
						</a>
                    </div>
                    
				</div>

               
               
			</div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
<?php  include('include/footer.php'); ?>        
        
    

    