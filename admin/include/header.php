<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Petrosafe Admin</title>
    <!-- Favicon icon 
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">-->
    <!-- Pignose Calender -->
    
    <!-- Chartist -->
    <!-- <link rel="stylesheet" href="./plugins/chartist/css/chartist.min.css">
    <link rel="stylesheet" href="./plugins/chartist-plugin-tooltips/css/chartist-plugin-tooltip.css"> -->
	
	<!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">-->
    <!-- Custom Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
	<link href="css/jquery.fancybox.css" rel="stylesheet">
	<link href="css/bootstrap-tagsinput.css" rel="stylesheet">
	<link href="css/select2.min.css" rel="stylesheet">
	<script src="./plugins/common/common.min.js"></script>
	<link href="./plugins/summernote/dist/summernote.css" rel="stylesheet">
	<link rel="stylesheet" href="css/jquery-ui.css">
	<link href="./plugins/sweetalert/css/sweetalert.css" rel="stylesheet">
	<script src="js/select2.min.js"></script>
	<!--<link rel="stylesheet" href="css/buttons.dataTables.min.css">-->
	<!--<script src="js/jquery-3.3.1.js"></script>-->
</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>






    <div id="main-wrapper">

     
     
     
     
        <div class="col-md-12 col-sm-12 header-section">
     
        <div class="nav-header">
            <div class="brand-logo">
                <a href="dashboard.php">
                    <b class="logo-abbr"><img src="./images/logo2.png" alt=""> </b>
                    <span class="logo-compact"><img src="./images/logo2.png" alt=""></span>
                    <span class="brand-title">
                        <img src="./images/logo2.png" alt="">
                    </span>
                </a>
            </div>
        </div>


        <div class="header">    
            <div class="header-content clearfix">
                
                <div class="nav-control">
                    <div class="hamburger">
                        <span class="toggle-icon"><i class="icon-menu"></i></span>
                    </div>
                </div>
                <div class="header-right">
                    <ul class="clearfix">
                       <li class="icons dropdown">
                            <div class="user-img c-pointer position-relative"   data-toggle="dropdown">
                                <span class="activity active"></span>
                                <img src="images/user/1.png" height="40" width="40" alt="">
                            </div>
                            <div class="drop-down dropdown-profile animated fadeIn dropdown-menu" style="transform: translate3d(0px, 45px, 0px) !important;">
                                <div class="dropdown-content-body">
                                    <ul>
                                        
                                        <li><a href="logout.php"><i class="icon-key"></i> <span>Logout</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            
                
            </div>
        </div>
        
        </div>
 
 
 
 
 
 
        <div class="nk-sidebar" style="top:55px !important;">           
            <div class="nk-nav-scroll">
                <ul class="metismenu" id="menu">
					<li>
                        <a href="dashboard.php" aria-expanded="false">
                            <i class="fa fa-sitemap menu-icon" aria-hidden="true"></i><span class="nav-text">Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="payments.php" aria-expanded="false">
                            <i class="fa fa-money menu-icon" aria-hidden="true"></i><span class="nav-text">Payments</span>
                        </a>
                    </li>
					<li>
                        <a href="reading.php" aria-expanded="false">
                            <i class="icon-speedometer menu-icon"></i><span class="nav-text">Meter Reading</span>
                        </a>
                    </li>
                    <li>
                        <a href="bulk-sms.php" aria-expanded="false">
                            <i class="fa fa-mobile menu-icon" aria-hidden="true" style="font-size:1.75em !important;"></i><span class="nav-text">Bulk Sms</span>
                        </a>
                    </li>
					<li>
                        <a href="bulk-email.php" aria-expanded="false">
                            <i class="fa fa-envelope menu-icon" aria-hidden="true" ></i><span class="nav-text">Bulk Email</span>
                        </a>
                    </li>
					<li>
                        <a href="whatsapp.php" aria-expanded="false">
                            <i class="fa fa-whatsapp menu-icon" aria-hidden="true" ></i><span class="nav-text">Whatsapp</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
     
     