<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TimeSlotsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('time_slots')->insert([
            'from_slot' => '09:00:00',
            'to_slot' => '12:00:00',
            'booking_count' => 10,
            'slot_status' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('time_slots')->insert([
            'from_slot' => '12:00:00',
            'to_slot' => '15:00:00',
            'booking_count' => 10,
            'slot_status' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);


        DB::table('time_slots')->insert([
            'from_slot' => '15:00:00',
            'to_slot' => '18:00:00',
            'booking_count' => 10,
            'slot_status' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }

}
