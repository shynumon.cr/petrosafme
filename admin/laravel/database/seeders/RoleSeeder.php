<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\Admin;
use App\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class RoleSeeder extends Seeder
{
    public function run(): void
    {
        $permission = new Permission();
        $permission->name = 'create-post2';
        $permission->slug = 'create-post2';
        $permission->category = '1';
        $permission->save();

        $role = new Role();
        $role->name = 'admin2';
        $role->slug = 'admin2';
        $role->save();
        $role->permissions()->attach($permission);
        //$permission->roles()->attach($role);

        $permission = new Permission();
        $permission->name = 'create-user2';
        $permission->slug = 'create-user2';
        $permission->category = '2';
        $permission->save();

        $role = new Role();
        $role->name = 'user2';
        $role->slug = 'user2';
        $role->save();
        $role->permissions()->attach($permission);
        //$permission->roles()->attach($role);

        $admin = Role::where('name', 'admin2')->first();
        $userRole = Role::where('name', 'user2')->first();
        $create_post = Permission::where('name', 'create-post2')->first();
        $create_user = Permission::where('name', 'create-user2')->first();

        $admin = new Admin();
        $admin->LoginName = 'Admin2';
        $admin->email = 'admin2@gmail.com';
        $admin->password = bcrypt('admin2');
        $admin->save();
        $admin->roles()->attach($admin);
        $admin->permissions()->attach($create_post);

        $user = new Admin();
        $user->LoginName = 'User2';
        $user->username = 'User2';
        $user->email = 'user2@gmail.com';
        $user->password = bcrypt('user2');
        $user->save();
        $user->roles()->attach($userRole);
        $user->permissions()->attach($create_user);
    }
}