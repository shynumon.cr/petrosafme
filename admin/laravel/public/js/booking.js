$(document).ready(function(){
    $("#new_building_div").hide();
    $("#new_flat__div").hide();
    $("#shifting_to_div").hide();
    $("#gas_available_div").hide();
    $("#availabilty_div").hide();
    $("#dewa_conect_div").hide();
    $("#pay_mode_div").hide();
    $("#eid_front_div").hide();
    $("#eid_back_div").hide();
    $("#tenacy_div").hide();
    $("#meter_div").hide();
    $("#gas_cooker_div").hide();
    $("#gas_contract_div").hide();
    $("input[name='booking_type']").change(function (e) {
        var booking_type = $(this).val();
        if(booking_type =='CON'){

            $("#pay_mode_div").hide();
            $("#availabilty_div").hide();
            $("#gas_contract_div").hide();
            $("#new_building_div").hide();
            $("#new_flat__div").hide();
            $("#shifting_to_div").hide();

            $("#gas_available_div").show();
            $("#dewa_conect_div").show();
            $("#pay_mode_div").show();
            $("#eid_front_div").show();
            $("#eid_back_div").show();
            $("#tenacy_div").show();
            $("#meter_div").show();
            $("#gas_cooker_div").show();
        } else if(booking_type =='DIS'){
            $("#gas_available_div").hide();
            $("#dewa_conect_div").hide();
            $("#pay_mode_div").hide();
            $("#tenacy_div").hide();
            $("#meter_div").hide();
            $("#gas_cooker_div").hide();
            $("#new_building_div").hide();
            $("#new_flat__div").hide();
            $("#shifting_to_div").hide();

            $("#eid_front_div").show();
            $("#eid_back_div").show();
            $("#gas_contract_div").show();
            $("#availabilty_div").show();

        } else if(booking_type =='SHIF'){

            $("#dewa_conect_div").hide();
            $("#pay_mode_div").hide();
            $("#tenacy_div").hide();
            $("#meter_div").hide();
            $("#gas_cooker_div").hide();
            $("#gas_contract_div").hide();
            $("#availabilty_div").hide();


            $("#new_building_div").show();
            $("#new_flat__div").show();
            $("#shifting_to_div").show();
            $("#eid_front_div").show();
            $("#eid_back_div").show();
            $("#tenacy_div").show();
            $("#pay_mode_div").show();
            $("#gas_available_div").show();
            $("#dewa_conect_div").show();

        } else if(booking_type =='RECON'){
            $("#dewa_conect_div").hide();
            $("#pay_mode_div").hide();
            $("#tenacy_div").hide();
            $("#meter_div").hide();
            $("#gas_cooker_div").hide();
            $("#gas_contract_div").hide();
            $("#availabilty_div").hide();
            $("#new_building_div").hide();
            $("#new_flat__div").hide();
            $("#shifting_to_div").hide();
            $("#eid_front_div").hide();
            $("#eid_back_div").hide();
            $("#pay_mode_div").hide();
            $("#gas_available_div").hide();
            $("#dewa_conect_div").hide();
        }
    });
});

// *********************************************************

// submit booking
$("#booking_form").validate({
    ignore: [],
    rules: {
        customer_name: {
            required: true
        },
        contact_no: {
            required: true
        },

        address: {
            required: true
        },
        flat_no: {
            required: true
        },
        building_name: {
            required: true
        },
        whatsapp_no: {
            required: true
        },
        service_date: {
            required: true
        },
        booking_type: {
            required: true
        },
        new_building_name: {
            required: function(element) {
                return $("#booking_type").val() === 'SHIF';
            }
        },
        new_flat_no: {
            required: function(element) {
                return $("#booking_type").val() === 'SHIF';
            }
        },
        shifting_to: {
            required: function(element) {
                return $("#booking_type").val() === 'SHIF';
            }
        },
        team: {
            required: true
        },
        area: {
            required: true
        },
        slot_id: {
            required: true
        },
        gas_available: {
            required: function(element) {
                return $("#booking_type").val() === 'CON'||$("#booking_type").val() === 'SHIF';
            }
        },
        availability: {
            required: function(element) {
                return $("#booking_type").val() === 'DIS';
            }
        },
        active_dewa_connection: {
            required: function(element) {
                return $("#booking_type").val() === 'CON'||$("#booking_type").val() === 'SHIF';
            }
        },
        payment_mode: {
            required: function(element) {
                return $("#booking_type").val() === 'CON'||$("#booking_type").val() === 'SHIF';
            }
        },
        comments: {
            required: true
        },
        eid_front: {
            required: function(element) {
                return $("#booking_type").val() === 'CON' ||$("#booking_type").val() === 'DIS'
                ||$("#booking_type").val() === 'SHIF';
            }
        },
        eid_back: {
            required: function(element) {
                return $("#booking_type").val() === 'CON' ||$("#booking_type").val() === 'DIS'
                ||$("#booking_type").val() === 'SHIF';
            }
        },
        tenancy: {
            required: function(element) {
                return $("#booking_type").val() === 'CON'||$("#booking_type").val() === 'SHIF';
            }
        },
        meter_picture: {
            required: function(element) {
                return $("#booking_type").val() === 'CON';
            }
        },
        gas_cooker_photo: {
            required: function(element) {
                return $("#booking_type").val() === 'CON';
            }
        },
        gas_contract_type: {
            required: function(element) {
                return $("#booking_type").val() === 'DIS';
            }
        },
        gas_contract_image: {
            required: function(element) {
                return $("#booking_type").val() === 'DIS';
            }
        },

    },
    messages: {

    },
    errorPlacement: function(err, ele) {
        err.insertAfter(ele);
    },
    submitHandler: function(form, element) {
        $(".submit_button").html('<i class="fa fa-spinner fa-spin"></i> Please Wait').prop(
            'disabled', true);
        fm = new FormData(form);
        $.ajax({
            url: 'add-booking',
            type: 'post',
            data: fm,
            cache: false,
            processData: false,
            contentType: false,
            success: function(data) {
                console.log(data);
                if (data.status == 'success') {
                    $("#user_form")[0].reset();
                    $(".add-member-popup-wrapper").hide(500);
                    studentTable.draw();
                    swal("Success!", data.message, "success");
                    $(".submit_buttom").html('Save').prop('disabled', false);
                } else {
                    $(".add-member-popup-wrapper").hide(500);
                    $(".submit_buttom").html('Save').prop('disabled', false);
                    swal("Error!", data.message, "error");
                }
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText);
            }
        });
    }
});
