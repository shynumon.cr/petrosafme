<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

function jsVersion()
{
    // useful to force reload js files to a new version
    // sometimes cached js files make issues
    // change return number to a new one to force reload in client side
    // usage ↓
    // <script type="text/javascript" src="{{ URL::asset('js/example.js?v='.jsVersion()) }}"></script>
    return 1.1;
}

function cssVersion()
{
    // useful to force reload css files to a new version
    // sometimes cached css files make issues
    // change return number to a new one to force reload in client side
    // usage ↓
    // <link rel="stylesheet" type="text/css" href="{{URL::asset('css/example.css?v='.cssVersion())}}"/>
    return 1.0;
}