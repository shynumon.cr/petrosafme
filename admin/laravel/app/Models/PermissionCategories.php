<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionCategories extends Model
{
    protected $table = 'permission_categories';
    protected $fillable = [
        'id','permission_category'
    ];
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    public function permission()
    {
        return $this->belongsTo(Permission::class,'category');
    }   
}
