<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public function roles() {

	   return $this->belongsToMany(Role::class,'roles_permissions');
	       
	}

	public function users() {

	   return $this->belongsToMany(User::class,'users_permissions');
	       
	}
	/*public function categoryval() {

	   //eturn $this->belongsTo(PermissionCategories::class,'id');
	   return $this->belongsToMany(PermissionCategories::class,'id');
	       
	}*/
	/*public function categories()
    {
        return $this->hasOne(PermissionCategories::class,'category');
    } */
}
