<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Area;
use App\Models\Team;
use App\Models\Booking;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use stdClass;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class BookingController extends Controller
{
    public function view_booking()
    {
        $category = Booking::select('id', 'team_id', 'area_id','team_name','from_slot','to_slot')->where('booking_status', 1)->get();
        // print_r($customers);
        // die();
        return view('bookings.booking_list');
    }
    // ***********************************************************************************************
    public function list_Booking(Request $req)
    {
        // Log::info('Post Data:'.json_encode($req->all()));
        $p = DB::table('bookings');
        if (isset($req->from_date) && $req->from_date != '')
            $p = $p->whereRaw('date(created_at) >= "' . $req->from_date . '"');
        if (isset($req->to_date) && $req->to_date != '')
            $p = $p->whereRaw('date(created_at) <= "' . $req->to_date . '"');
        if (isset($req->keywordsearch) && $req->keywordsearch != '')
            $p = $p->where('team_name', 'like', '%' . $req->keywordsearch . '%');
        if (isset($req->sub_status) && $req->sub_status != '') {
            if ($req->sub_status == 'name.asc') {
                $p = $p->orderBy('id', 'ASC');
            } else if ($req->sub_status == 'name.desc') {
                $p = $p->orderBy('id', 'DESC');
            } else if ($req->sub_status == 'created_at.asc') {
                $p = $p->orderBy('created_at', 'ASC');
            } else {
                $p = $p->orderBy('created_at', 'DESC');
            }
        } else {
            $p = $p->orderBy('id', 'DESC');
        }

        $filtered = $p->count();
        $p = $p->offset($req->start)->limit($req->length);
        $p = $p->get();
        $total = DB::table('bookings')->count();
        $products = [];
        $j = $req->start;
        foreach ($p as $k => $v) {
            $action = '<div class="tooltip-ation-main">
            <i class="fa fa-cog"></i>
            <div class="tooltip-ation">
                <div class="tp-arrow-back"></div>
                <div class="tp-arrow"></div>
                <ul>';
            $action .= '<li class="view-action"><a data-url=""><label onclick=view_booking("' . $v->id . '");><i class="fa fa-eye"></i> View</label></a></li>';
            $action .= '<li class="view-action"><a data-url=""><label onclick=edit_booking("' . $v->id . '");><i class="fa fa-pencil"></i> Edit</label></a></li>';
            $action .= '</ul>
            </div>
        </div>';
            $products[] = [
                'slno' => ($j + 1),
                'id' => $v->id,
                'reference_id ' => $v->reference_id,
                'team_id' => $v->team_id,
                'area_id' => $v->area_id,
                'area_name' => $v->area_name,
                'team_name' => $v->team_name,
                'from_slot' => $v->from_slot,
                'to_slot ' => $v->to_slot,
                'service_date  ' => $v->service_date,
                'booking_count' => $v->booking_count,
                'booking_status' => $v->booking_status,
                'booking_type' => $v->booking_type,
                'created_date' => date('d-m-Y', strtotime($v->created_at)),
                'created_time' => date('h:i A', strtotime($v->created_at)),
                'updated_date' => date('d-m-Y', strtotime($v->updated_at)),
                'updated_time' => date('h:i A', strtotime($v->updated_at)),
                'action' => $action,
            ];
            $j++;
        }
        return ['data' => $products, 'draw' => $req->draw, 'recordsTotal' => $total, 'recordsFiltered' => $filtered];
    }
    // *********************************************************************************
    public function add_edit_Booking_create(Request $req)
    {
        if (isset($req->id) && $req->id != '') {
            return $this->edit_Booking($req);
        }
        /************************************************* */ // validate
        $niceNames = [
            'title' => 'Booking Title',
            'arabicTitle' => 'Arabic Title',
        ];
        $validator = Validator::make($req->all(), [
            'title' => 'bail|required|unique:Bookings,title',
            'arabicTitle' => 'bail|required|unique:Bookings,arabicTitle',
            'BookingMessage' => 'bail|required',
            'arabicBookingMessage' => 'bail|required',
            'category_name' => 'bail|required',
            'priority_name' => 'bail|required',
            'customer_name' => 'bail|required',
        ], [], $niceNames);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }
        /************************************************* */
        $latestBooking = DB::table('Bookings')->latest()->first();
        $latestReference = $latestBooking ? $latestBooking->BookingReference : 'NELT-' . date('Y') . '-0000';
        $referenceParts = explode('-', $latestReference);
        $counter = intval(end($referenceParts));
        $newCounter = str_pad($counter + 1, 4, '0', STR_PAD_LEFT);
        $newreference = 'NELT-' . date('Y') . '-' . $newCounter;

        $p = [
            'BookingReference' => $newreference,
            'BookingCategoryId' => $req->category_name,
            'title' => $req->title,
            'arabicTitle' => $req->arabicTitle,
            'BookingMessage' => $req->BookingMessage,
            'arabicBookingMessage' => $req->arabicBookingMessage,
            'priorityId' => $req->priority_name,
            'customerId' => $req->customer_name,
            'created_at' => now(),
            'updated_at' => now(),
        ];

        $id = DB::table('Bookings')->insertGetId($p);
        if ($id) {
            return ['status' => 'success', 'message' => 'Booking Added Successfully !'];
        } else {
            return ['status' => 'error', 'message' => 'Booking Adding Failed !'];
        }
    }
    // *****************************************************************************
    function get_Booking(Request $req)
    {
        // log::info('post id '.json_encode($req->all()));
        $p = Booking::Find($req->id);
        if ($p) {
            return ['status' => 'success', 'data' => $p];
        } else {
            return response()->json(['status' => 'error', 'message' => 'No Booking Id Found']);
        }
    }
    // ********************************************************************************
    public function edit_Booking(Request $req)
    {
        /************************************************* */ // validate
        $niceNames = [
            'title' => 'Booking Title',
            'arabicTitle' => 'Arabic Title',
        ];
        $validator = Validator::make($req->all(), [
            'title' => 'bail|required|unique:Bookings,title,' . $req->id,
            'arabicTitle' => 'bail|required|unique:Bookings,arabicTitle,' . $req->id,
            'BookingMessage' => 'bail|required',
            'arabicBookingMessage' => 'bail|required',
            'category_name' => 'bail|required',
            'priority_name' => 'bail|required',
            'customer_name' => 'bail|required',
        ], [], $niceNames);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }
        /************************************************* */
        $p = Booking::Find($req->id);

        if (!$p) {
            return response()->json(['status' => 'error', 'message' => 'Booking not found']);
        }

        $p->title = $req->title;
        $p->arabicTitle = $req->arabicTitle;
        $p->BookingMessage = $req->BookingMessage;
        $p->arabicBookingMessage = $req->arabicBookingMessage;
        $p->BookingCategoryId = $req->category_name;
        $p->priorityId = $req->priority_name;
        $p->customerId = $req->customer_name;
        $p->updated_at = now();
        $p->save();
        return ['status' => 'success', 'message' => 'Booking Updated Successfully'];
    }
    // ************************************************************************************
    function delete_Booking_new(Request $req)
    {
        $p = Booking::Find($req->id);
        $p->status = 0;
        $p->save();
        return ['status' => 'success', 'message' => 'Booking Removed Successfully'];
    }
    // ************************************************************************************

    public function view_Booking_details($id)
    {
        try {
            // log::info('post id ' . json_encode($id));

            $query = Booking::leftjoin('Booking_categories', 'Bookings.BookingCategoryId', '=', 'Booking_categories.id')
                ->leftjoin('Booking_priorities', 'Bookings.priorityId', '=', 'Booking_priorities.id')
                ->leftjoin('customers', 'Bookings.customerId', '=', 'customers.id')
                ->select(
                    'Bookings.*',
                    'Booking_categories.categoryName',
                    'Booking_categories.categoryArabicName',
                    'Booking_priorities.priorityName',
                    'Booking_priorities.priorityArabicName',
                    'customers.id as customer_id',
                    'customers.name',
                    'customers.nameArabic',
                    'customers.mobileNumber',
                    'customers.emailId',
                    'customers.image',
                )
                ->where('Bookings.id', $id);
            // ->where('Booking_categories.status', 1)
            // ->where('Booking_priorities.status', 1)
            // ->where('customers.status', 1);

            $Bookingdetails = $query->first();

            if ($Bookingdetails) {
                // Log::info("Result: " . json_encode($Bookingdetails));
                $comments = Booking::where('BookingId', $id)->where('commentStatus', 1)->get();
                return view('Booking_list.Booking_details', compact('Bookingdetails', 'comments'));
            } else {
                Log::info("No Booking details found for ID: " . $id);

                return response()->json(['status' => 'error', 'message' => 'No Booking details found.']);
            }
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => 'An error occurred.']);
        }
    }

    // ************************************************************************************************

    public function updateBookingstatus(Request $request, $id)
    {
        // log::info('post status'.json_encode($request->all()));
        $Booking = Booking::find($id);

        if (!$Booking) {
            return response()->json(['status' => 'error', 'message' => 'Booking not found']);
        }

        $Booking->BookingStatus = $request->input('status');
        $Booking->save();

        return response()->json(['status' => 'success', 'message' => 'Booking status updated successfully']);
    }


    // *******************************************************************************************************

    public function Booking_reply(Request $request)
    {
        $userid = Auth::id();
        $customer = $request->input('customer_id');
        // log::info('session userid' . $userid);
        // log::info('Post Booking Reply' . json_encode($request->all()));
        // *******************************************************
        $validator = Validator::make($request->all(), [
            'Booking_id' => 'required|numeric',
            'reply' => 'required|string',
            'customer_id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
        }
        // *********************************************************

        $inn =  Booking::create([
            'BookingId' => $request->input('Booking_id'),
            'comments' => $request->input('reply'),
            'arabicComments' => $request->input('reply'),
            'commentFrom' => 'A',
            'customerId' => $customer,
            'userId' => $userid,
            'created_at' => now()
        ]);

        if ($inn) {
            $message = "Booking Reply Added Successfully";
            return response()->json(['success' => true, 'message' => $message]);
        } else {
            $message = "Something Went Wrong";
            return response()->json(['success' => false, 'message' => $message]);
        }
    }
    // ****************************************************************************************

    public function get_comments(Request $req)
    {

        $p = Booking::Find($req->id);
        if ($p) {
            return ['status' => 'success', 'data' => $p];
        } else {
            return response()->json(['status' => 'error', 'message' => 'No Booking Id Found']);
        }
    }

    // ****************************************************************************************

    public function edit_comments(Request $request)
    {
        // Log::info('post updated comments'.json_encode($request->all()));
        $comment = Booking::find($request->input('comment_id'));

        if (!$comment) {
            return response()->json(['status' => 'error', 'message' => 'Comment not found']);
        }

        $comment->comments = $request->input('edited_comment');
        $comment->arabicComments = $request->input('edited_comment');
        $comment->updated_at = now();
        $comment->save();

        return response()->json(['status' => 'success', 'message' => 'Comment updated successfully']);
    }

    // ****************************************************************************************

    function delete_comments(Request $req)
    {
        $p = Booking::Find($req->id);
        $p->commentStatus = 2;
        $p->save();
        return ['status' => 'success', 'message' => 'Booking Comment Removed Successfully'];
    }




    // *********************************************************************************************
    public function check_crm_exist(Request $request){
        try {
            $Booking = Booking::find($request->id);
            $subject=$Booking->title;
            $subject=$Booking->arabicTitle;
            $message=$Booking->BookingMessage;

            if (!$Booking) {
                return response()->json(['status' => 'error', 'message' => 'Booking not found.']);
            }

            $customer = Booking::find($Booking->customerId);
            if (!$customer) {
                return response()->json(['status' => 'error', 'message' => 'Customer not found for this Booking.']);
            }

            $crmBooking = $this->crmBookingcreate($customer->mobileNumber,$subject,$message);
            $crmBookings = json_decode($crmBooking);
                // log::info('API RESPONSE'.json_encode($crmBookings));
                // print_r($crmBookings);exit();
                if($crmBookings->success == true && $crmBookings->result->Booking_no != "")
                {
                    $Booking->crmBookingId = $crmBookings->result->Booking_no;
                    $Booking->save();
                    return response()->json(['status' => 'success', 'message' => 'Booking pushed to CRM successfully.']);
                }
                 else {
                    return response()->json(['status' => 'error', 'message' => 'Booking ID already exists in CRM.']);
                }

        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }

    // *********************************************************************************************

    public function crmBookingcreate($mobileNumber,$subject,$message)
    {
        $phoneNumber = '0'.substr($mobileNumber, -9);
        $password = "umsd9tt";

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://crm.bevatel.com/tlt-gas-api/addBooking.php?password='.$password.'&contactmobile='.urlencode($phoneNumber).'&title='.urlencode($subject).'&description='.urlencode($message),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

    // *******************************************************************
}

