<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\RolesPermissions;
use App\Models\Permission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use stdClass;
use Illuminate\Support\Facades\Log ;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Redirect;
class RoleController extends Controller
{
    public function index()
    {
        $all_category_permission=[];
        $all_permission_categories = Permission::select('category')->groupBy('category')->get();
        foreach ($all_permission_categories as $key => $value) {
//dd($value->categoryval);
            $all_permission_under_category = Permission::where('category','=',$value->category)->get();
            foreach ($all_permission_under_category as $k => $perm) {
               $all_category_permission[$value->category][$perm->slug]= $perm->name;
            }
        }
        return view('roles.roleList',compact('all_category_permission'));
    }
    public function list_roles(Request $req)
    {
        $p = DB::table('roles')->select('roles.*');

        if (isset($req->from_date) && $req->from_date != '')
            $p = $p->whereRaw('date(roles.created_at) >= "' . $req->from_date . '"');
        if (isset($req->to_date) && $req->to_date != '')
            $p = $p->whereRaw('date(roles.created_at) <= "' . $req->to_date . '"');
        if (isset($req->keywordsearch) && $req->keywordsearch != '')
            $p = $p->where('roles.name', 'like', '%' . $req->keywordsearch . '%');
        $filtered = $p->count();
        $p = $p->offset($req->start)->limit($req->length);
        $p = $p->orderBy('id', 'DESC')->get();
        $total = DB::table('roles')->count();
        $roles = [];
        $j = $req->start;
        foreach ($p as $k => $v) {
            $action = '<div class="tooltip-ation-main">
            <i class="fa fa-cog"></i>
            <div class="tooltip-ation">
                <div class="tp-arrow-back"></div>
                <div class="tp-arrow"></div>
                <ul>';
        

            $action .= '<li class="view-action"><a data-url=""><label onclick=edit_role("' . $v->id . '");><i class="fa fa-pencil"></i> Edit</label></a></li>';
        

            $action .= '<li class="edit-action"><a data-url=""><label onclick=delete_role("' . $v->id . '");><i class="fa fa-trash"></i> Delete</label></a></li>';
            
            $action .= '</ul>
            </div>
        </div>';
            $roles[] = [
                'slno' => ($j + 1),
                
                'name' => $v->name,
                'slug' => $v->slug,
                'addedtime' => date('d-m-Y h:i A', strtotime($v->created_at)),
                'action' => $action,
            ];
            $j++;
        }
        return ['data' => $roles, 'draw' => $req->draw, 'recordsTotal' => $total, 'recordsFiltered' => $filtered];
    }
    function get_role(Request $req)
    {
        $p = Role::with('permissions')->Find($req->id);
        /*$assigned_permissions = RolesPermissions::where('role_id','=',$req->id)->get();
        $assigned_permissions_slug_array=[];
        if(!empty($assigned_permissions)){
            foreach ($assigned_permissions as $key => $value) {
                 $assigned_permission_id_array = Permission::where('id','=',$value->permission_id)->get();
                 foreach ($assigned_permission_id_array as $key => $val) {
                    $assigned_permissions_slug_array[] = $val->slug;
                 }
                 
            }
        }*/

        if ($p) {
            return ['status' => 'success', 'data' => $p];
        } else {
            return response()->json(['status' => 'error', 'message' => 'No Role Found']);
        }
    }
    function add_edit_role(Request $req)
    {
        if (isset($req->id) && $req->id != '') {
            return $this->edit_role($req);
        }
        $nickNames = [
        ];
        $validator = Validator::make($req->all(), [
            'name' => 'required',
            'slug' => 'required|unique:roles'
        ], $nickNames);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }
        $role = new Role();
        $permissions = $req->permissions;
        //$p = new stdClass();
        $role->created_at = date('Y-m-d H:m:s');
        $role->updated_at = date('Y-m-d H:m:s');
        $role->name = $req->name;
        $role->slug = $req->slug;
        //$role = (array) $role;
       // $us_id = DB::table('roles')->insertGetId($role);
        $updated_role = $role->save();
        if($updated_role){
                    /*try*/
            if(!empty($permissions)){
                foreach ($permissions as $key => $perm) {
                    $each_permission = Permission::where('slug',$perm)->first();
                    $role->permissions()->attach($each_permission);
                }
            }
            
            return ['status' => 'success', 'message' => 'Role Added Successfully'];
        }else{
             return ['status' => 'failure', 'message' => 'Role not added'];//redirect('roles-list')->with('response','Failed to add Role');
        }
    }
    public function edit_role(Request $req)
    {
        log::info('post datas' . json_encode($req->all()));
        $nickNames = [
        ];
        $validator = Validator::make($req->all(), [
            'name' => 'required',
            'slug' => 'required|unique:roles,slug,'. $req->id.',id',
        ], $nickNames);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }
        $role = Role::find($req->id);
        $permissions = $req->permissions;
        $role->updated_at = now();
        $role->name = $req->name;
        $role->slug = $req->slug;
        $updated_role =$role->save();
        if($updated_role){
            if(!empty($permissions)){
                foreach ($permissions as $key => $perm) {
                    $each_permission = Permission::where('slug',$perm)->first();
                    $role->permissions()->detach($each_permission);
                    $role->permissions()->attach($each_permission);
                    //$role->permissions()->attach($each_permission);
                }
            }
        
            return ['status' => 'success', 'message' => 'Role Updated Successfully'];
        }else{
            return ['status' => 'failure', 'message' => 'Role Not Updated'];
        }
    }
    function delete_role(Request $req)
    {
        DB::beginTransaction();
        try {
            Role::where('id', $req->id)
            ->delete();
            return ['status' => 'success', 'message' => 'Role Removed Successfully!'];
        } catch (\Throwable $e) {
            DB::rollback();
            return ['status' => 'error', 'message' => 'Role Remove Failed !'];
        }
    }
}
