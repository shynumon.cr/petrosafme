<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Area;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use stdClass;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class AreaController extends Controller
{
    public function area_view(Request $req)
    {
        $teams= DB::table('teams')->select('id as t_id', 'team_name')
        ->where('team_status',1)->get();
        return view('areas.area_list',compact('teams'));
    }

    // ***********************************************************************************************
    public function list_area(Request $req)
    {
        $p = DB::table('areas')->where('area_status', 1);
        if (isset($req->from_date) && $req->from_date != '')
            $p = $p->whereRaw('date(created_at) >= "' . $req->from_date . '"');
        if (isset($req->to_date) && $req->to_date != '')
            $p = $p->whereRaw('date(created_at) <= "' . $req->to_date . '"');
        if (isset($req->keywordsearch) && $req->keywordsearch != '')
            $p = $p->where('area_name', 'like', '%' . $req->keywordsearch . '%');
        if (isset($req->sub_status) && $req->sub_status != '') {
            if ($req->sub_status == 'name.asc') {
                $p = $p->orderBy('area_name', 'ASC');
            } else if ($req->sub_status == 'name.dsc') {
                $p = $p->orderBy('area_name', 'DESC');
            } else if ($req->sub_status == 'created_at.asc') {
                $p = $p->orderBy('created_at', 'ASC');
            } else {
                $p = $p->orderBy('created_at', 'DESC');
            }
        } else {
            $p = $p->orderBy('id', 'DESC');
        }
        $filtered = $p->count();
        $p = $p->offset($req->start)->limit($req->length);
        $p = $p->get();
        $total = DB::table('areas')->where('area_status', 1)->count();
        $products = [];
        $j = $req->start;
        foreach ($p as $k => $v) {
            $action = '<div class="tooltip-ation-main">
            <i class="fa fa-cog"></i>
            <div class="tooltip-ation">
                <div class="tp-arrow-back"></div>
                <div class="tp-arrow"></div>
                <ul>';
            $action .= '<li class="view-action"><a data-url=""><label onclick=edit_area("' . $v->id . '");><i class="fa fa-pencil"></i> Edit</label></a></li>';
            $action .= '<li class="edit-action"><a data-url=""><label onclick=delete_area("' . $v->id . '");><i class="fa fa-trash"></i> Delete</label></a></li>';
            $action .= '</ul>
            </div>
        </div>';
            $products[] = [
                'slno' => ($j + 1),
                'id' => $v->id,
                'area_status' => $v->area_status,
                'area_name' => $v->area_name,
                'description' => $v->description,
                'team_id' => $v->team_id,
                'team_name' => $v->team_name,
                'created_date' => date('d-m-Y', strtotime($v->created_at)),
                'created_time' => date('h:i A', strtotime($v->created_at)),
                'updated_date' => date('d-m-Y', strtotime($v->updated_at)),
                'updated_time' => date('h:i A', strtotime($v->updated_at)),
                'action' => $action,
            ];
            $j++;
        }
        return ['data' => $products, 'draw' => $req->draw, 'recordsTotal' => $total, 'recordsFiltered' => $filtered];
    }

    public function add_edit_area(Request $req)
    {
        // log::info('PostArea :'.json_encode($req->all()));
        if (isset($req->id) && $req->id != '') {
            return $this->edit_area($req);
        }
        /************************************************* */ // validate
        $niceNames = [
            'area_name' => 'Area Name',
            'description' => 'Description',
            'team_id' => 'Team Name',
        ];
        $validator = Validator::make($req->all(), [
            'area_name' => 'bail|required|unique:areas,area_name',
            'description' => 'bail|required',
            'team_id' => 'bail|required',
        ], [], $niceNames);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }
        /************************************************* */
        $p = new stdClass();
        $p->created_at = now();
        $p->area_name = $req->area_name;
        $p->description = $req->description;
        $p->team_id = $req->team_id;
        $p->team_name = $req->team_name;
        $p = (array) $p;
        $id = DB::table('areas')->insertGetId($p);
        if ($id) {
            return ['status' => 'success', 'message' => 'Area Added Successfully !'];
        } else {
            return ['status' => 'error', 'message' => 'Area Adding Failed !'];
        }
    }

    function get_area(Request $req)
    {
        $p =Area::Find($req->id);
        return ['status' => 'success', 'data' => $p];
    }

    public function edit_area(Request $req)
    {
        /************************************************* */ // validate
        $niceNames = [
            'area_name' => 'Area Name',
            'description' => 'Description',
        ];
        $validator = Validator::make($req->all(), [
            'area_name' => 'bail|required|unique:areas,area_name',
            'description' => 'bail|required',
        ], [], $niceNames);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }
        /************************************************* */
        $p =Area::Find($req->id);
        $p->updated_at = date('Y-m-d H:m:s');
        $p->area_name = $req->area_name;
        $p->description = $req->description;
        $p->team_id = $req->team_id;
        $p->team_name = $req->team_name;
        $p->save();
        return ['status' => 'success', 'message' => 'Area Updated Successfully'];
    }

    function delete_area(Request $req)
    {
        $p =Area::Find($req->id);
        $p->area_status = 0;
        $p->deleted_at =now();
        $p->save();
        return ['status' => 'success', 'message' => 'Area Removed Successfully'];
    }
}

