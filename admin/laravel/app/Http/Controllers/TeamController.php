<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Team;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use stdClass;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class TeamController extends Controller
{
    public function team_view(Request $req)
    {
        return view('teams.team_list');
    }

    // ***********************************************************************************************
    public function list_team(Request $req)
    {
        $p = DB::table('teams')->where('team_status', 1);
        if (isset($req->from_date) && $req->from_date != '')
            $p = $p->whereRaw('date(created_at) >= "' . $req->from_date . '"');
        if (isset($req->to_date) && $req->to_date != '')
            $p = $p->whereRaw('date(created_at) <= "' . $req->to_date . '"');
        if (isset($req->keywordsearch) && $req->keywordsearch != '')
            $p = $p->where('team_name', 'like', '%' . $req->keywordsearch . '%');
        if (isset($req->sub_status) && $req->sub_status != '') {
            if ($req->sub_status == 'name.asc') {
                $p = $p->orderBy('team_name', 'ASC');
            } else if ($req->sub_status == 'name.dsc') {
                $p = $p->orderBy('team_name', 'DESC');
            } else if ($req->sub_status == 'created_at.asc') {
                $p = $p->orderBy('created_at', 'ASC');
            } else {
                $p = $p->orderBy('created_at', 'DESC');
            }
        } else {
            $p = $p->orderBy('id', 'DESC');
        }
        $filtered = $p->count();
        $p = $p->offset($req->start)->limit($req->length);
        $p = $p->get();
        $total = DB::table('teams')->where('team_status', 1)->count();
        $products = [];
        $j = $req->start;
        foreach ($p as $k => $v) {
            $action = '<div class="tooltip-ation-main">
            <i class="fa fa-cog"></i>
            <div class="tooltip-ation">
                <div class="tp-arrow-back"></div>
                <div class="tp-arrow"></div>
                <ul>';
            $action .= '<li class="view-action"><a data-url=""><label onclick=edit_team("' . $v->id . '");><i class="fa fa-pencil"></i> Edit</label></a></li>';
            $action .= '<li class="edit-action"><a data-url=""><label onclick=delete_team("' . $v->id . '");><i class="fa fa-trash"></i> Delete</label></a></li>';
            $action .= '</ul>
            </div>
        </div>';
            $products[] = [
                'slno' => ($j + 1),
                'id' => $v->id,
                'team_status' => $v->team_status,
                'team_name' => $v->team_name,
                'contact_no' => $v->contact_no,
                'created_date' => date('d-m-Y', strtotime($v->created_at)),
                'created_time' => date('h:i A', strtotime($v->created_at)),
                'updated_date' => date('d-m-Y', strtotime($v->updated_at)),
                'updated_time' => date('h:i A', strtotime($v->updated_at)),
                'action' => $action,
            ];
            $j++;
        }
        return ['data' => $products, 'draw' => $req->draw, 'recordsTotal' => $total, 'recordsFiltered' => $filtered];
    }

    public function add_edit_team(Request $req)
    {
        // log::info('Post Team :'.json_encode($req->all()));
        if (isset($req->id) && $req->id != '') {
            return $this->edit_team($req);
        }
        /************************************************* */ // validate
        $niceNames = [
            'team_name' => 'Team Name',
        ];
        $validator = Validator::make($req->all(), [
            'team_name' => 'bail|required|unique:teams,team_name',
            'contact_no' => 'bail|required',
        ], [], $niceNames);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }
        /************************************************* */
        $p = new stdClass();
        $p->created_at = now();
        $p->team_name = $req->team_name;
        $p->contact_no = $req->contact_no;
        $p = (array) $p;
        $id = DB::table('teams')->insertGetId($p);
        if ($id) {
            return ['status' => 'success', 'message' => 'Team Added Successfully !'];
        } else {
            return ['status' => 'error', 'message' => 'Team Adding Failed !'];
        }
    }

    function get_team(Request $req)
    {
        $p = Team::Find($req->id);
        return ['status' => 'success', 'data' => $p];
    }

    public function edit_team(Request $req)
    {
        /************************************************* */ // validate
        $niceNames = [
            'team_name' => 'Team Name',
            'contact_no' => 'Contact Number',
        ];
        $validator = Validator::make($req->all(), [
            'team_name' => 'bail|required|unique:teams,team_name,' . $req->id,
            'contact_no' => 'required',
        ], [], $niceNames);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }
        /************************************************* */
        $p = Team::Find($req->id);
        $p->updated_at = date('Y-m-d H:m:s');
        $p->team_name = $req->team_name;
        $p->contact_no = $req->contact_no;
        $p->save();
        $a=Area::where('team_id', $p->id)->update(['team_name' => $req->team_name]);
        $ts=TimeSlotTeamManagement::where('team_id', $p->id)->update(['team_name' => $req->team_name]);
        return ['status' => 'success', 'message' => 'Team Updated Successfully'];
    }

    function delete_team(Request $req)
    {
        $p = Team::Find($req->id);
        $p->team_status = 0;
        $p->deleted_at =now();
        $p->save();
        return ['status' => 'success', 'message' => 'Team Removed Successfully'];
    }
}
