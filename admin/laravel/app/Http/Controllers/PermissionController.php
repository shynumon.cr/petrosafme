<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Permission;
use App\Models\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use stdClass;
use Illuminate\Support\Facades\Log ;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Redirect;
class PermissionController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('permissions.permissionList',compact('categories')
                    );
    }
    public function list_permissions(Request $req)
    {
        $p = DB::table('permissions')->select('permissions.*');

        if (isset($req->from_date) && $req->from_date != '')
            $p = $p->whereRaw('date(permissions.created_at) >= "' . $req->from_date . '"');
        if (isset($req->to_date) && $req->to_date != '')
            $p = $p->whereRaw('date(permissions.created_at) <= "' . $req->to_date . '"');
        if (isset($req->keywordsearch) && $req->keywordsearch != '')
            $p = $p->where('permissions.name', 'like', '%' . $req->keywordsearch . '%');
        $filtered = $p->count();
        $p = $p->offset($req->start)->limit($req->length);
        $p = $p->orderBy('id', 'DESC')->get();
        $total = DB::table('permissions')->count();
        $permissions = [];
        $j = $req->start;
        foreach ($p as $k => $v) {
            $action = '<div class="tooltip-ation-main">
            <i class="fa fa-cog"></i>
            <div class="tooltip-ation">
                <div class="tp-arrow-back"></div>
                <div class="tp-arrow"></div>
                <ul>';
            $action .= '<li class="view-action"><a data-url=""><label onclick=edit_permission("' . $v->id . '");><i class="fa fa-pencil"></i> Edit</label></a></li>';
            $action .= '<li class="edit-action"><a data-url=""><label onclick=delete_permission("' . $v->id . '");><i class="fa fa-trash"></i> Delete</label></a></li>';
            $action .= '</ul>
            </div>
        </div>';
            $permissions[] = [
                'slno' => ($j + 1),
                
                'name' => $v->name,
                'slug' => $v->slug,
                'category' => $v->slug,
                'addedtime' => date('d-m-Y h:i A', strtotime($v->created_at)),
                'action' => $action,
            ];
            $j++;
        }
        return ['data' => $permissions, 'draw' => $req->draw, 'recordsTotal' => $total, 'recordsFiltered' => $filtered];
    }
    function get_permission(Request $req)
    {
        $p = Permission::Find($req->id);
        if ($p) {
            return ['status' => 'success', 'data' => $p];
        } else {
            return response()->json(['status' => 'error', 'message' => 'No Permission Found']);
        }
    }
    function add_edit_permission(Request $req)
    {
        if (isset($req->id) && $req->id != '') {
            return $this->edit_permission($req);
        }
        $nickNames = [
        ];
        $validator = Validator::make($req->all(), [
            'name' => 'required',
            'category' => 'required',
            'slug' => 'required|unique:permissions'
        ], $nickNames);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }
        $p = new stdClass();
        $p->created_at = date('Y-m-d H:m:s');
        $p->updated_at = date('Y-m-d H:m:s');
        $p->name = $req->name;
        $p->slug = $req->slug;
        $p->category = $req->category;
        $p = (array) $p;
        $us_id = DB::table('permissions')->insertGetId($p);
        return ['status' => 'success', 'message' => 'Permission Added Successfully'];
    }
    public function edit_permission(Request $req)
    {
        log::info('post datas' . json_encode($req->all()));
        $nickNames = [
        ];
        $validator = Validator::make($req->all(), [
            'name' => 'required',
            'category' => 'required',
            'slug' => 'required|unique:permissions,slug,'. $req->id.',id',
        ], $nickNames);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }
        $permission = Permission::find($req->id);
        $permission->updated_at = now();
        $permission->name = $req->name;
        $permission->slug = $req->slug;
        $permission->category = $req->category;
        $permission->save();
        return ['status' => 'success', 'message' => 'Permission Updated Successfully'];
    }
    function delete_permission(Request $req)
    {
        DB::beginTransaction();
        try {
            Permission::where('id', $req->id)
            ->delete();
            return ['status' => 'success', 'message' => 'Permission Removed Successfully!'];
        } catch (\Throwable $e) {
            DB::rollback();
            return ['status' => 'error', 'message' => 'Permission Remove Failed !'];
        }
    }
}