<!doctype html>
<html lang="en">
<head>
    <title>{{ $title ?? 'Petrosafe' }}</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="theme-color" content="#d5e5f4">
    @include('includes.header_assets')
</head>
<body>
    <script>
        var _base_url = '{{ url('') }}';
    </script>
    <div class="wrapper-main pb-0">
        @yield('content')
        @include('includes.footer_assets')
</div>
</body>
</html>