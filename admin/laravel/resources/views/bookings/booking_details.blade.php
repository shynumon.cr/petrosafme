@extends('layouts.main')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="row cm-content-section m-0">
        <div class="col-12 page-title-main">
            <ul>
                <li>
                    <h4 class="MyriadPro-Bold">Ticket Details</h4>
                </li>
                <li class="float-right">Last Login: <span
                        class="text-blue bold">{{ date('F d, Y h:i a', strtotime(Auth()->user()->lastLoginTime)) }}</span>
                </li>
            </ul>
        </div>
        <!--page-title-main end-->

        <div class="col-12 cm-content-main overflow-hidden ">
            <div class="row">
                <div class="col-md-12 m-auto p-3">
                    <div class="row m-0 pt-3">
                        <div class="col-sm-6 text-center"> <img src="{{ URL::asset('images/user-photo.png') }}"alt=""
                                class="ticket-user-photo">
                            <h4 class="text-center">{{ $ticketdetails->ticketReference }}</h4>
                            <h3 class="text-center">User</h3>
                            <p class="text-center"><strong>{{ $ticketdetails->name }}</strong></p>
                        </div>
                        <div class="row m-0 col-sm-6">
                            <div class='col-sm-6 tickets-details-tmb'>
                                <div class='col-12 tickets-det-box'>
                                    <h5><strong>Title</strong></h5>
                                    <p>{{ $ticketdetails->title }}</p>
                                    <p><strong> Arabic </strong> ({{ $ticketdetails->arabicTitle }}) </p>
                                </div>
                            </div>
                            <div class='col-sm-6 tickets-details-tmb'>
                                <div class='col-12 tickets-det-box'>
                                    <h5><strong>Category Name</strong> </h5>
                                    <p>{{ $ticketdetails->categoryName }}</p>
                                    <p><strong> Arabic </strong> ({{ $ticketdetails->categoryArabicName }}) </p>
                                </div>
                            </div>
                            <div class='col-sm-6 tickets-details-tmb'>
                                <div class='col-12 tickets-det-box'>
                                    <h5><strong>Priority Name</strong></h5>
                                    <p> {{ $ticketdetails->priorityName }} </p>
                                    <p> <strong> Arabic </strong> ({{ $ticketdetails->priorityArabicName }}) </p>
                                </div>
                            </div>
                            <div class='col-sm-6 tickets-details-tmb'>
                                <div class='col-12 tickets-det-box'>
                                    <h5><strong>Status</strong></h5>
                                    <p> @php
                                        $status = '';
                                        $bg = '';

                                        switch ($ticketdetails->ticketStatus) {
                                            case 0:
                                                $status = 'Pending';
                                                $bg = 'danger';
                                                break;
                                            case 1:
                                                $status = 'Active';
                                                $bg = 'success';
                                                break;
                                            case 2:
                                                $status = 'Closed';
                                                $bg = 'primary';
                                                break;
                                            case 3:
                                                $status = 'Cancelled';
                                                $bg = 'warning';
                                                break;
                                            default:
                                                $status = 'Unknown Status';
                                                $bg = 'secondary';
                                        }
                                    @endphp </p>
                                    <span
                                        class="btn btn-xl text-white badge badge-{{ $bg }}">{{ $status }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 ticket-details-description">
                            <form id="ticketform">
                                @csrf
                                <h4><strong>Description</strong></h4>
                                <div class="form-control border-0" id="description" name="description">
                                    {{ $ticketdetails->ticketMessage }} </div>
                                <h4 class="pt-4"><strong>Comments</strong></h4>
                                <div class="comment-container">
                                    @foreach ($comments as $comment)
                                        <div class="comment">
                                            <div class="circle"></div>
                                            <div class="comment-content">
                                                {{ $comment->comments }}-{{ $comment->arabicComments }}
                                                <button style="display: none" type='button'
                                                    class="rounded-pill edit_cmt_btn btn btn-primary btn-sm"
                                                    onclick="get_comment('{{ $comment->id }}')"
                                                    data-comment-id="{{ $comment->id }}">Edit</button>
                                                <button style="display: none" type='button'
                                                    class="rounded-pill del_cmt_btn btn btn-danger btn-sm"
                                                    onclick="delete_comment('{{ $comment->id }}')"
                                                    data-comment-id="{{ $comment->id }}">Delete</button>
                                                <div class="comment-user">
                                                    @if ($comment->commentFrom == 'U')
                                                        <i class="fa fa-user" style="color: green;">&nbsp; User</i>
                                                    @elseif ($comment->commentFrom == 'A')
                                                        <i class="fa fa-user" style="color: grey;">&nbsp;Admin</i>
                                                    @endif &nbsp; <span class="fa fa-clock-o"
                                                        style="color: gray;"></span>&nbsp;{{ $comment->created_at->format('d-M-Y g:i A') }}
                                                </div>
                                            </div>
                                            <div class="line"></div>
                                        </div>
                                    @endforeach
                                </div>
                                <p class="pt-4"><strong>Reply</strong></p>
                                <textarea class="col-sm-6 form-control" id="reply" name="reply" rows="4"></textarea>
                                <div id="sbmt_div" class="pb-3 pt-3 align-items-center ">
                                    <button type="submit" id="submitbtn"
                                        class="mr-3 btn-lg bg-success rounded-pill border-0 text-light">Submit</button>

                                    <button id="verifyBtn"
                                        class=" mr-3 btn-lg bg-primary rounded-pill border-0 text-light">Verify</button>
                                    <button id="closeBtn"
                                        class=" mr-3 btn-lg bg-danger rounded-pill border-0 text-light">Close
                                        Ticket</button>
                                    <button id="cancelBtn"
                                        class=" btn-lg bg-warning rounded-pill border-0 text-light">Cancel Ticket</button>
                                </div>
                            </form>
                            <!-- <div class="col-sm-12 p-0 mt-3">
           <button id="verifyBtn" class=" mr-3 btn-lg bg-primary rounded-pill border-0 text-light">Verify</button>
           <button id="closeBtn" class=" mr-3 btn-lg bg-danger rounded-pill border-0 text-light">Close Ticket</button>
           <button id="cancelBtn" class=" btn-lg bg-warning rounded-pill border-0 text-light">Cancel Ticket</button>
          </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- edit popup --}}
    <div id="modal-main" class="add-member-popup-wrapper">
        <div class="add-member-popup-section d-flex">
            <div class="add-member-popup-main col-sm-4 mx-auto">
                <div class="close-btn"><img src="{{ URL::asset('images/white-close.png') }}" alt=""></div>
                <h5 class="MyriadPro-Bold modal_title">Edit Comment</h5>
                <div class="col-12 step-cont" id="cont1">
                    <form id="edit-comment_form" class="row">
                        <div class="col-md-12"> @csrf
                            <div class="col-sm-12 cm-field-main cm-field pl-0 pr-0">
                                <textarea class="field-big medium" id="edited_comment" name="edited_comment" rows="4"></textarea>
                            </div>
                            <input type="hidden" id="comment_id" name="comment_id">
                            <div class="row m-0">
                                <div class="col-sm-12 cm-field-btn p-0 m-0">
                                    <button id='update_cmt_btn' type="submit"
                                        class="field-btn w-auto Primary">Update</button>

                                    <button id='can_cmt_btn' type="button" class="field-btn w-auto">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @php
        $ticketStatus = $ticketdetails->ticketStatus;
        $customerid = $ticketdetails->customer_id;
    @endphp
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ URL::asset('js/jquery.validate.js') }}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var tickstatus = {{ $ticketStatus }};
        $(document).ready(function() {

            if (tickstatus == 0) {
                $('#closeBtn, #cancelBtn, #reply,#submitbtn,.comment-container').hide();
                $('#reply').prev('p').hide();
                $('#verifyBtn').show();
            } else if (tickstatus == 1) {
                $('#verifyBtn').hide();
                $('#closeBtn, #cancelBtn, #reply, #sbmt_div').show();
                $('#reply').prev('p').show();
                $('.edit_cmt_btn,.del_cmt_btn').show();

            } else if (tickstatus == 2) {
                $('#verifyBtn').html('Closed').addClass('bg-primary').prop('disabled', true);
                $('#closeBtn, #cancelBtn, #reply, #sbmt_div').hide();
                $('#reply').prev('p').hide();
            } else if (tickstatus == 3) {
                $('#verifyBtn').html('Cancelled').addClass('bg-info').prop('disabled', true);
                $('#closeBtn, #cancelBtn, #reply, #sbmt_div').hide();
                $('#reply').prev('p').hide();
            }

            $('#verifyBtn').click(function(e) {
                e.preventDefault();
                updateStatus(1);
            });

            $('#closeBtn').click(function(e) {
                e.preventDefault();
                updateStatus(2);

            });

            $('#cancelBtn').click(function(e) {
                e.preventDefault();
                updateStatus(3);
            });
            // ********************************************************
            function updateStatus(newstatus) {
                $.ajax({
                    url: '{{ route('update-ticket-status', $ticketdetails->id) }}',
                    type: 'post',
                    data: {
                        status: newstatus
                    },
                    dataType: 'json',
                    success: function(data) {
                        location.reload();
                        console.log('Status updated successfully');
                    },
                    error: function(data) {
                        console.error('Error updating status');
                    }
                });
            }
            // ***********************************************************************************

            // Function to close the modal
            $('.close-modal').click(function() {
                $('#modal-main').hide();
            });

            // *******************************************************************

            $('#ticketform').submit(function(e) {
                e.preventDefault();
                var reply = $('#reply').val();
                var customer_id = {{ $customerid }};

                $.ajax({
                    url: '{{ route('ticket-reply') }}',
                    type: 'post',
                    data: {
                        '_token': $('meta[name=csrf-token]').attr('content'),
                        'ticket_id': {{ $ticketdetails->id }},
                        'reply': reply,
                        'customer_id': customer_id
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.success) {
                            swal("Success!", data.message, "success").then(function() {
                                location.reload();
                            });
                        } else {
                            swal("Error!", data.message, "error");
                        }
                    },
                    error: function(data) {
                        swal("Error!", "Error submitting reply", "error");
                    }
                });
            });
            // *******************************************************************
            $("#edit-comment_form").validate({
                ignore: [],
                rules: {
                    edited_comment: {
                        required: true
                    },
                },
                messages: {
                    edited_comment: "Enter a Comment",
                },

                errorPlacement: function(err, ele) {
                    err.insertAfter(ele);
                },
                submitHandler: function(form, element) {
                    $("#update_cmt_btn").html('<i class="fa fa-spinner fa-spin"></i> Please Wait').prop(
                        'disabled', true);
                    $('#can_cmt_btn').prop('disabled', true);

                    var editedComment = $('#edited_comment').val();
                    var commentId = $('#comment_id').val();

                    $.ajax({
                        url: '{{ Url('edit-comment') }}',
                        type: 'post',
                        data: {
                            '_token': $('meta[name=csrf-token]').attr('content'),
                            'comment_id': commentId,
                            'edited_comment': editedComment
                        },
                        dataType: 'json',
                        success: function(data) {
                            if (data.status == 'success') {
                                $('#modal-main').hide(500);
                                swal("Success!", data.message, "success").then(function() {
                                    location.reload();
                                });
                            } else {
                                $('#modal-main').hide(500);
                                swal("Error!", data.message, "error");
                            }
                        },
                        error: function(data) {
                            swal("Error!", "Error updating comment", "error");
                        }
                    });
                }
            })
            // *********************************************************************

            const circles = document.querySelectorAll('.circle');
            circles.forEach(circle => {
                circle.style.backgroundColor = getRandomColor();
            });

            function getRandomColor() {
                const letters = '0123456789ABCDEF';
                let color = '#';
                for (let i = 0; i < 6; i++) {
                    color += letters[Math.floor(Math.random() * 16)];
                }
                return color;
            }

        });

        function get_comment(id) {
            event.preventDefault();
            $.ajax({
                url: '{{ Url('get-comment') }}',
                type: 'post',
                data: {
                    id: id
                },
                success: function(data) {
                    // console.log(data);
                    if (data.status == 'success') {
                        $('#edited_comment').val(data.data.comments);
                        $('#comment_id').val(data.data.id);
                        $('#modal-main').show(500);

                        var textarea = document.getElementById('edited_comment');
                        textarea.focus();
                        textarea.setSelectionRange(textarea.value.length, textarea.value.length);
                    } else {
                        swal("Error!", data.message, "error");
                    }
                }

            })
        }
        // ***************************************************************************************
        // Function to handle deleting a comment
        function delete_comment(id) {
            swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this Comment !",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '{{ Url('delete-comment') }}',
                            type: 'post',
                            data: {
                                id: id
                            },
                            success: function(data) {
                                if (data.status == 'success') {
                                    swal("Success!", data.message, "success").then(function() {
                                        location.reload();
                                    });
                                } else {
                                    swal("Error!", data.message, "error");
                                }
                            },
                            error: function(data) {
                                swal("Error!", data.statusText, "error");
                            }

                        })
                    }
                });
        }
    </script>
    <style>
        .error {
            color: red;
            font-size: 13px;
            display: block;
            text-align: left;
        }

        .comment {
            display: flex;
            align-items: center;
            margin-bottom: 15px;
            position: relative;
        }

        .circle {
            width: 10px;
            height: 10px;
            border-radius: 50%;
            margin-right: 10px;
        }

        .line {
            position: absolute;
            left: calc(3.5px);
            top: 2rem;
            width: 2px;
            height: 100%;
            background-color: darkgrey;
        }

        .comment-content {
            flex-grow: 1;

        }
    </style>
@endpush
