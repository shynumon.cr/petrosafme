@extends('layouts.main')
@section('content')
    {{-- <?php
    $per = getPermission();
    ?> --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="add-member-popup-wrapper">
        <div class="add-member-popup-section d-flex">
            <div class="add-member-popup-main col-sm-5 mx-auto">
                <div class="close-btn"><img src="{{ URL::asset('images/white-close.png') }}" alt=""></div>
                <h5 class="MyriadPro-Bold modal_title">Add Event</h5>
                <div class="col-12 step-cont" id="cont1">
                    <form id="time_slot_form" class="row" method="post">
                        <div class="row m-0">
                            <div class="col-sm-6 cm-field-main cm-field">
                                <p>Date<span class="text-danger">*</span></p>
                                <input type="date" readonly name="slot_date" type="text" id="slot_date">
                            </div>

                            <div class="col-sm-6 cm-field-main cm-field">
                                <p>Team<span class="text-danger">*</span></p>
                                <select name="team_id" id="team_id" class="input-field">
                                    <option value="">-- Select Team --</option>
                                    @foreach ($team as $teamItem)
                                        <option value="{{ $teamItem->team_id }}">{{ $teamItem->team_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-6 cm-field-main cm-field">
                                <p>Timings<span class="text-danger">*</span></p>
                                <select name="slot_id" id="slot_id" class="input-field">
                                    <option value="">-- Select Time Slot --</option>
                                    @foreach ($time_slots as $slot)
                                        <option value="{{ $slot->slot_id }}">
                                            {{ date('h:i A', strtotime($slot->from_slot)) }} -
                                            {{ date('h:i A', strtotime($slot->to_slot)) }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-sm-6 cm-field-main cm-field">
                                <p>Booking Count<span class="text-danger">*</span></p>
                                <input class="form-control no-arrow" type="number" name="booking_count" id="booking_count"
                                    class="input-field">
                                <input type="hidden" name="team_slot_id" id="team_slot_id">
                            </div>


                            <div class="col-sm-12 cm-field-btn">
                                <button type="submit"
                                    class="submit_button field-btn CM font-weight-normal w-auto">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>







    <div class="row cm-content-section m-0">
        <div class="col-12 page-title-main">
            <ul>

                {{-- <li class="float-right">Last Login: <span
                        class="text-blue bold">{{ date('F d, Y h:i a', strtotime(Auth()->user()->lastLoginTime)) }}</span>
                </li> --}}

                <ul class="list-group">
                    <h5>Time Slot Information</h5>
                    @foreach ($time_slots as $slot)
                    <li class="list-group-item">
                        {{ date('h:i A', strtotime($slot->from_slot)) }} -
                        {{ date('h:i A', strtotime($slot->to_slot)) }}
                        <span class="badge p-2 badge-primary badge-pill">{{ $slot->booking_count }}</span>
                    </li>
                    @endforeach
                </ul>
                <li>
                    <h4 class="MyriadPro-Bold">Event View</h4>
                </li>
            </ul>
        </div>
        <!--page-title-main end-->

        <div class="col-12 cm-content-main">
            <div id='calendar'></div>

            <!--cm-content-main end-->
        </div>
        <!--cm-content-section end-->
    @endsection
    @push('scripts')
        <script src="https://cdn.jsdelivr.net/npm/fullcalendar@6.1.11/index.global.min.js"></script>
        <script src='https://cdn.jsdelivr.net/npm/fullcalendar/index.global.min.js'></script>
        <script type="text/javascript" src="{{ URL::asset('js/jquery.validate.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/time_slot.js') }}"></script>
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // *****************************************
            // CALENDAR
            // document.addEventListener('DOMContentLoaded', function() {
                $(document).ready(function(){


                const calendarEl = document.getElementById('calendar');
                window.calendar = new FullCalendar.Calendar(calendarEl, {
                    initialView: 'dayGridMonth',
                    dateClick: function(info) {
                        $('#team_id').val('');
                        $('#slot_id').val('');
                        $('#booking_count').val('');
                        $('#team_slot_id').val('');
                        const clickedDate = new Date(info.dateStr);
                        const today = new Date();
                        today.setHours(0, 0, 0, 0);
                        clickedDate.setHours(0, 0, 0, 0);
                        if (clickedDate >= today) {
                            document.getElementById('slot_date').value = info.dateStr;

                            show_booking_form();
                        } else {
                            swal({
                                title: "Error!",
                                text: "Past dates are not allowed.",
                                icon: "error",
                            });
                        }
                    },
                    events: [
                        @foreach ($time_slot_team as $event)
                            {
                                title: '{{ $event->team_name }}',
                                start: '{{ $event->slot_date }}',
                                team_id: '{{ $event->team_id }}',
                                slot_id: '{{ $event->slot_id }}',
                                team_slot_id: '{{ $event->team_slot_id }}',
                                booking_count: '{{ $event->booking_count }}',
                                from_slot: '{{ date('h:i A', strtotime($event->from_slot)) }}',
                                to_slot: '{{ date('h:i A', strtotime($event->to_slot)) }}',
                                extendedProps: {
                                    team_name: '{{ $event->team_name }}',
                                    team_id: '{{ $event->team_id }}',
                                    slot_id: '{{ $event->slot_id }}',
                                    booking_count: '{{ $event->booking_count }}'
                                }
                            },
                        @endforeach
                    ],
                    eventContent: function(arg) {
                        return {
                            html: '<div class="fc-content">' +
                                '<span class="fc-title">' + arg.event.title + '</span>' +
                                '<div class="fc-time-slot">Time: ' + arg.event.extendedProps.from_slot +
                                ' - ' + arg.event.extendedProps.to_slot + '</div>' +
                                '<div class="fc-time-slot">Booking Count: ' + arg.event.extendedProps
                                .booking_count + '</div>' +
                                '</div>'
                        };
                    },
                    eventClick: function(info) {
                        $('.add-member-popup-wrapper').show();
                        $('#slot_date').val(info.event.startStr);
                        $('#team_id').val(info.event.extendedProps.team_id);
                        $('#slot_id').val(info.event.extendedProps.slot_id);
                        $('#booking_count').val(info.event.extendedProps
                            .booking_count);
                        $('#team_slot_id').val(info.event.extendedProps
                            .team_slot_id);
                    }
                });
                window.calendar.render();
            });

            // *********************************************************

            function show_booking_form() {
                $(".add-member-popup-wrapper").show(500);
            }

            // *********************************************************
            function submit_booking() {
                const name = document.getElementById('name').value;
                document.getElementById('booking_form').style.display = 'none';
            };
        </script>
        <style>
            .error {
                color: red;
                font-size: 13px;
                display: block;
                text-align: left;
            }

            input[type=number]::-webkit-inner-spin-button,
            input[type=number]::-webkit-outer-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            input[type=number] {
                -moz-appearance: textfield;
            }
        </style>
    @endpush
