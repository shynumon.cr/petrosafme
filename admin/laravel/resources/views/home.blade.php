@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')
<div class="row cm-content-section m-0">
    <div class="col-12 page-title-main pl-0 pr-0">
        <ul>
            <li>
                <h4 class="MyriadPro-Bold">Dashboard</h4>
            </li>
            <li class="float-right">Last Login: <span
                    class="text-blue bold">{{ date('F d, Y h:i a', strtotime(Auth()->user()->lastLoginTime)) }}</span>
            </li>
        </ul>
    </div>
</div>
<div class="card-body">
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    @role('admin')
        {{ __('You are admin') }}
    @endrole
    @role('manager')
        {{ __('You are user') }}
    @endrole
<?php
echo "<pre>";
print_r(auth()->user()->permissions);
echo "<pre>";
    if(auth()->user()->can('list-user')){
        echo "you can List user";
    }else{
        echo "can't List user";
    }
?>
</div>
@endsection
@push('styles')
        <link rel="stylesheet" href="{{ URL::asset('css/animate.min.css') }}" />
    @endpush
    