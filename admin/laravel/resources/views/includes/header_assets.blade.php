<link rel="icon" type="image/png" href="{{URL::asset('images/logo2.png')}}"/>
<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/bootstrap.css')}}" >
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/style.css?v='.cssVersion())}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/font-awesome.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/owl.carousel.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/booking.css')}}"/>
@stack('styles')
