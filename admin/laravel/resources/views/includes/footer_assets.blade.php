<script type="text/javascript" src="{{URL::asset('js/jquery-3.6.0.min.js')}}"></script> 
<script type="text/javascript" src="{{URL::asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/common.js?v='.jsVersion())}}"></script>
<script type="text/javascript" src="{{ URL::asset('js/sweetalert.min.js') }}"></script>
<script type="text/javascript">
 $(document).ready(function() {
    $(".menu-icon").click(function(){
        $(".main-nav").toggle(500);
    });
    $(".top-dropdown-set").click(function(){
        $(".top-dropdown").toggle(500);
    });  
 });   
</script>
@stack('scripts')