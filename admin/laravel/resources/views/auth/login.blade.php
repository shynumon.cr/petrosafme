@extends('layouts.login')
@section('content')
 <div class="row login-section m-0">
        <div class="col-lg-8 col-md-6 login-left p-0">
            <h1 class="MyriadPro-Bold text-capital"><span class="MyriadPro-Regular">Welcome to</span><br>PetroSafe</h1>
        </div>
        <div class="col-lg-4 col-md-6 login-right d-flex mx-auto my-auto">

            <form method="POST" action="{{ route('login') }}" id="loginForm">
                @csrf
                <div class="login-main mx-auto">
                    <div class="col-sm-12 login-logo">
                        <img src="{{ URL::asset('images/logo.png') }}" class=" mx-auto">
                    </div>
@if($errors->any())
<div class="alert alert-danger pt5-pr10">
    {{ implode('', $errors->all(':message')) }}
</div>
@endif


                    <div class="col-sm-12 cm-field-main position-relative p-0">
                        <i class="fa fa-user"></i>
                        <input class="input-field @error('email') is-invalid @enderror" id="email" placeholder="Email" name="email" value="{{ old('email') }}" type="text" required autocomplete="email" autofocus>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-sm-12 cm-field-main position-relative p-0">
                        <i class="fa fa-unlock-alt"></i>
                        <input class="input-field @error('password') is-invalid @enderror" placeholder="Password" name="password" id="password" type="password" required autocomplete="current-password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="row remember-set m-0">
                        <div class="col-6 log-remb p-0">
                            <input name="remember" id="remember"  class="" type="checkbox" {{ old('remember') ? 'checked' : '' }}>
                            <label for="option-01"> <span></span> &nbsp; Remember Me</label>
                        </div>
                    </div>
                    <div id="msg_error"><p style="color:red;font-size:13px;">&nbsp;</p></div>
                    <div class="col-sm-12 cm-field-btn p-0 m-0">
                        <input type="submit" class="field-btn CM font-weight-normal" id="button_submit" value=" {{ __('Login') }}">
                        @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif
                    </div>
                </div>
            </form>

            <footer>
                <div class="az-section">
                    <a href="http://azinovatechnologies.com/" target="_blank">
                        <div class="azinova-logo"></div>
                    </a>
                    <p class="no-padding">Powered by :</p>
                    <div class="clear"></div>
                </div>
            </footer>
        </div>
    </div>

@endsection