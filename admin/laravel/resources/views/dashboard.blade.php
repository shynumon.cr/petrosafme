@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')
    <div class="row cm-content-section m-0">
        <div class="col-12 page-title-main pl-0 pr-0">
            <ul>
                <li>
                    <h4 class="MyriadPro-Bold">Dashboard</h4>
                </li>
                {{-- <li class="float-right">Last Login: <span
                        class="text-blue bold">{{ date('F d, Y h:i a', strtotime(Auth()->user()->lastLoginTime)) }}</span>
                </li> --}}
            </ul>
        </div>
        <!--page-title-main end-->
        <div class="col-12 dashboard-wrapper p-0">
            <div class="row dashboard-tmb-wrapper p-0">
                <div class="col-lg-7 col-md-6 p-0">
                    <div class="row m-0">
                        <div class="col-lg-6 col-md-6 dashboard-tmb-main">
                            <div class="col-12 blue-gradient dashboard-tmb">
                                <div class="dashboard-tmb-big-icon"><i class="fa fa-users f130"></i></div>
                                <div class="dashboard-tmb-title">Customers<i class="fa fa-users"></i></div>
                                <div class="dashboard-tmb-number"> <label id="pending_amount"></label>
                                    <!-- <span class="pending-amount float-right">AED <span id="outstanding_amount">0.00</span>
                                        <label>Pending</label>
                                    </span> -->
                                </div>
                                <!-- <div class="dashboard-tmb-date">Last Update:&nbsp;<span id="payment_updated">~</span></div> -->
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 dashboard-tmb-main">
                            <div class="col-12 green-gradient dashboard-tmb">
                                <div class="dashboard-tmb-big-icon"><i class="fa fa-bar-chart f95"></i></div>
                                <div class="dashboard-tmb-title">Users<i class="fa fa-bar-chart"></i></div>
                                <div class="dashboard-tmb-number">
                                    <div class="row m-0">
                                        <div class="col-6 p-0 text-white"><label
                                                id="today_sales_amount"></label></div>
                                        <!-- <div class="col-6 p-0 text-white text-right"><span>Count</span> <label
                                                id="today_sales_count">0.00
                                            </label></div> -->
                                    </div>
                                </div>
                                <!-- <div class="dashboard-tmb-date">Last Sale : <span id="today_last_sale">~</span></div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-6 dashboard-tmb-main">
                    <div class="row yellow-gradient dashboard-tmb m-0">
                        <div class="col-12 dashboard-tmb-title p-0">Bookings <i class="fa fa-money"></i></div>
                        <div class="col-4 p-0">
                            <div class="dashboard-tmb-number pb-0" style="line-height: 32px;">
                                {{-- <label id="collection-today">{{$pending_ticket}}</label> --}}
                            </div>
                            <div class="text-white">Pending</div>
                        </div>
                        <div class="col-4 p-0">
                            <div class="dashboard-tmb-big-icon"><i class="fa fa-money f109"></i></div>
                            <div class="dashboard-tmb-number pb-0" style="line-height: 32px;">
                                {{-- <label id="monthly_collection">{{$active_ticket}}</label> --}}
                            </div>
                            <div class="text-white">Active </div>
                        </div>
                        <div class="col-4 p-0">
                            <div class="dashboard-tmb-big-icon"><i class="fa fa-money f109"></i></div>
                            <div class="dashboard-tmb-number pb-0" style="line-height: 32px;">
                                {{-- <label id="monthly_collection">{{$closed_ticket}}</label> --}}
                            </div>
                            <div class="text-white">Inactive </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <canvas id="myChart" style="width:100%;max-width:100%"></canvas>
        </div>
    @endsection
    @push('styles')
        <link rel="stylesheet" href="{{ URL::asset('css/animate.min.css') }}" />
    @endpush
    @push('scripts')
        <script type="text/javascript" src="{{ URL::asset('js/owl.carousel.js?v=' . jsVersion()) }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/dashboard.js?v=' . jsVersion()) }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>



    @endpush
