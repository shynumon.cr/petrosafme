@extends('layouts.main')
@section('content')
{{-- <?php
    $per = getPermission();
?> --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="add-member-popup-wrapper">
        <div class="add-member-popup-section d-flex">
            <div class="add-member-popup-main mx-auto">
                <div class="close-btn"><img src="{{URL::asset('images/white-close.png')}}" alt=""></div>
                    <h5 class="MyriadPro-Bold modal_title">Add User Role</h5>
                    <div class="col-12 step-cont" id="cont1">
                        <form id="company_form" class="row" method="post">
                            <div class="col-md-12">
                                <div class="col-12 cm-field-main cm-field pl-0 pr-0">
                                    <p>Name</p>
                                    <input class="input-field" placeholder="" name="name" type="text">
                                    <input type="hidden" name="id">
                                </div>

                                <div class="col-12 cm-field-btn p-0">
                                    <button type="submit" class="submit_buttom field-btn CM font-weight-normal w-auto">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="common-popup-wrapper" id="delete_popup" style="display:none;">
            <div class="add-member-popup-section d-flex">
                <div class="add-member-popup-main mx-auto col-lg-4 col-md-6 col-sm-6">
                    <div class="close-btn"><img src="{{URL::asset('images/white-close.png')}}" alt=""></div>
                    <div class="common-popup-icon m-auto"><img src="{{URL::asset('images/alert.gif')}}" alt=""></div>
                    <h5 class="MyriadPro-Bold text-center">Are you sure to delete</h5>
                    <div class="common-popup-text">
                        <div class="col-12 cm-field-main cm-field p-0 pr-0 text-center mt-4">
                            <input type="button" class="field-btn CM font-weight-normal w-auto Danger" value="Delete">
                            <input type="button" class="field-btn font-weight-normal w-auto" value="Cancel">
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- Pop Up -->

        <div class="row cm-content-section m-0">
            <div class="col-12 page-title-main">
                <ul>
                    <li>
                        <h4 class="MyriadPro-Bold">User Roles</h4>
                    </li>
                    <li class="float-right">Last Login: <span class="text-blue bold">{{ date('F d, Y h:i a', strtotime(Auth()->user()->lastLoginTime)) }}</span></li>
                </ul>
            </div>
            <!--page-title-main end-->

            <div class="col-12 cm-content-main">
                <div class="col-12 table-main">
                    <div class="col-12 table-filter">
                        <ul>
                            <li>
                            <div class="cm-field-main m-0">
                                <p>Search Keyword</p>
                                <input class="input-field" placeholder="Search Here" id="search" type="text">
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>Start Date</p>
                                <input class="form-control input-field calendar" placeholder="Start Date" id="start_date" type="text" data-select="datepicker">
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>End Date</p>
                                <input class="form-control input-field calendar" placeholder="End Date" id="end_date"
                                    type="text" data-select="datepicker">
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p class="d-none d-md-block">&nbsp;</p>
                                <span class="CM filter-btn" onclick="search()"><i class="fa fa-search"></i>Search</span>
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>Sort by</p>
                                <select name=""  id="sub_status" class="time-slot" onchange="sortData()";>
                                    <option value="" data-url="">Select from list</option>
                                    <option value="name" data-url="">Name</option>
                                </select>
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>Show By</p>
                                <select name="show_by" id="show_by">
                                    @foreach ([10,25,50,100] as $it)
                                        <option {{$it==50?'selected':''}} value="{{$it}}">{{$it}} entries</option>
                                    @endforeach
                                </select>
                            </div>
                        </li>
                        {{-- @if(permissionCheck($per,'add_role')) --}}
                        <li class="float-right text-right width-auto">
                            <div class="cm-field-main m-0">
                                <p class="d-none d-md-block">&nbsp;</p>
                                <span class="CM filter-btn" onclick="add_company();"><i class="fa fa-plus"></i>New</span>
                            </div>
                        </li>
                        {{-- @endif --}}
                    </ul>
                </div>

                <table id="company_table" class="table table-sm table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>
                                Sl. No.
                            </th>
                            <th>
                                Role Name
                            </th>
                            <th class="action">
                                Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <!--cm-content-main end-->
        </div>
        <!--cm-content-section end-->
    @endsection
    @push('scripts')
        <script type="text/javascript" src="{{ URL::asset('js/jquery.validate.js') }}"></script>
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var studentTable = '';
            $(document).ready(function() {
                $("#start_date,#end_date").datepicker({autoclose: true});
                studentTable = $('#company_table').DataTable({
                    'bFilter': false,
                    'bLengthChange': false,
                    'pageLength': 50,
                    'processing': true,
                    'serverSide': true,
                    'bSort': false,
                    'serverMethod': 'post',
                    //  "bDestroy": true,
                    'ajax': {
                        'url': '{{ url('list-user-roles') }}',
                        'data': function(data) {
                            data.from_date = dmy($('#start_date').val());
                            data.to_date = dmy($('#end_date').val());
                            data.keywordsearch = $('#search').val();
                            data.sub_status = $('#sub_status').val();
                        },
                        "complete": function(json, type) {               // data sent from controllerr
                            var _resp = $.parseJSON(json.responseText);
                        }
                    },
                    'columns': [{
                            data: 'slno'
                        },
                        {
                            data: 'name'
                        },
                        {
                            data: 'action'
                        },
                    ]
                });

                $("#company_form").validate({
                    ignore: [],
                    rules: {
                        name: {
                            required: true
                        },
                    },
                    errorPlacement: function(err, ele) {
                        err.insertAfter(ele);
                    },
                    submitHandler: function(form, element) {
                        $(".submit_buttom").html('<i class="fa fa-spinner fa-spin"></i> Please Wait').prop(
                            'disabled', true);
                        fm = new FormData(form);
                        $.ajax({
                            url: '{{ Url('add-user-role') }}',
                            type: 'post',
                            data: fm,
                            cache: false,
                            processData: false,
                            contentType: false,
                            success: function(data) {
                                if (data.status == 'success') {
                                    $("#company_form")[0].reset();
                                    $(".add-member-popup-wrapper").hide(500);
                                    studentTable.draw();
                                    swal("Success!", data.message, "success");
                                    $(".submit_buttom").html('Save').prop('disabled', false);

                                } else
                                    swal("Error!", data.message, "error");
                            }
                        })
                    }
                })

                $('#show_by').change(function(){
                    studentTable.page.len($(this).val()).draw();
                })
            });

            function add_company() {
                $(".submit_buttom").html('Save');
                $('[name=id]').val('');
                $('.modal_title').html('Add User Role');
                $("#company_form")[0].reset();
                $(".add-member-popup-wrapper").show(500);
            }

            function delete_product(id) {
                swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this Company!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: '{{ Url('delete-user-role') }}',
                                type: 'post',
                                data: {
                                    id: id
                                },
                                success: function(data) {
                                    if (data.status == 'success') {
                                        studentTable.draw();
                                        swal("Success!", data.message, "success");
                                    } else {
                                        swal("Error!", data.message, "error");
                                    }
                                }

                            })
                        }
                    });
            }

            function view_product(id) {
                location.href = '{{ Url('view-products') }}/' + id;
            }

            function search() {
                studentTable.draw();
            }
            function sortData() {
                studentTable.draw();
            }

            function edit_product(id) {
                $.ajax({
                    url: '{{ Url('get-user-role') }}',
                    type: 'post',
                    data: {
                        id: id
                    },
                    success: function(data) {
                        if (data.status == 'success') {
                            put_fields(data.data);
                        } else {
                            swal("Error!", data.message, "error");
                        }
                    }

                })
            }

            function put_fields(a) {
                $("[name=name]").val(a.roleName);
                $("[name=id]").val(a.id);
                $('.modal_title').html('Edit User Role');
                $(".submit_buttom").html('Update');
                $(".add-member-popup-wrapper").show(500);
            }

            function dmy(a){
                if(a=='')
                return '';
                a=a.split('/');
                b=[];
                b.push(a[2]);
                b.push(a[0]);
                b.push(a[1]);
                b=b.join('-');
                return b;
            }

            function setpermission(a){
                window.location.href = a;
            }
        </script>
        <style>
.error {
    color:red;
    font-size:13px;
    display: block;
    text-align: left;
}
</style>
    @endpush
