@extends('layouts.main')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">




  

    <div class="row cm-content-section m-0">
        <div class="col-12 page-title-main">
            <ul>
                <li>
                    <h4 class="MyriadPro-Bold">{{$role->role}}</h4>
                </li>
                <li class="float-right">Last Login: <span class="text-blue bold">October 12-2021 02:37pm</span></li>
            </ul>
        </div>
        <!--page-title-main end-->

        <div class="col-12 cm-content-main">


            <div class="col-12 table-main">
                <div class="col-12 step-cont"  id="cont1">
                    <div class="row m-0">
  
                        <div class="col-12 member-view-cont-secion p-0">
                            <div class="row m-0">
                                 <div class="col-lg-8 col-md-12 p-0">
                                      <div class="row m-0">
                                           <div class="col-lg-3 col-md-12 member-photo-section-main">
                                            <div class="col-12 member-photo-set p-0">
                                                            
                                                <div class="col-12 member-photo"></div>
                                               <div class="col-12 member-name text-center">
                                                    <!-- <h5 class="text-blue">{{$role->role}}<br> -->
                                                    {{-- <span class="text-black">Lorem Ipsum</span> --}}
                                                  </h5>
                                               </div>
                                          </div>
                                           </div>
                                           
                                           
                                      </div>
                                 </div>
                                 
                                 
                                 <div class="col-lg-4 col-md-12 member-amount-section-main">
                                      
                                 </div>
                            </div>
                        </div>
                         
                    </div>
               </div>


               <label for="">All Permission</label>
               <div class="switch">

                   <input id="all_permission" type="checkbox" class="switch-input" />

                   <label for="all_permission" class="switch-label">Switch</label>

               </div>

               <div class="table-responsive grid_table">

                    

                <table class="table" id="permission_table">

              
                <tbody>
                    @foreach($per as $k => $v)
                    <tr>
                        <td>
                                {{$v['name']}}
                        </td>
                        @foreach($v['per'] as $vv)
                        <td>
                            {{$vv->name}}
    
                            <div class="switch">
    
                                <input id="all_permission{{$k.$vv->name}}" {{array_walk($up,function($val,$key,$id){if($val['per']==$id )echo 'checked ';},$vv->id)}} data-id="{{$vv->id}}" type="checkbox" class="switch-input" />
             
                                <label for="all_permission{{$k.$vv->name}}" class="switch-label">Switch</label>
             
                            </div>
                        </td>
                        @endforeach                                      
                    </tr>
                   @endforeach

                </tbody>
                </table>

            </div>

             
            <div class="row w-100">

                <div class="col"><button class="btn btn-primary" id="update_permission">Update Permisssion</button></div>

            </div>


            </div>
            <!--cm-content-main end-->
        </div>
        <!--cm-content-section end-->

    @endsection
    @push('scripts')
        <script type="text/javascript" src="{{ URL::asset('js/jquery.validate.js') }}"></script>

        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            
              $(document).ready(function(){
                $("#all_permission").change(function(){
                        if($(this).prop('checked'))
                        $("input[type = 'checkbox']").prop('checked',true);
                        else
                        $("input[type = 'checkbox']").prop('checked',false);
                        });



                        $("#update_permission").click(function(){
                    per=[];
                    $("input[type = 'checkbox']:not('#all_permission')").each(function(i,v){
                        per.push({id:$(this).data('id'),val:$(this).prop('checked')});
                    });
                    per={'per':per,id:window.location.href.split('/').pop()};
                    $.ajax({
                            url: '{{ Url('update-permission') }}',
                            type: 'post',
                            data: per,
                            success: function(data) {
                                if (data.status == 'success') {
                                    swal("Success!", data.message, "success");

                                } else
                                    swal("Error!", data.message, "error");
                            }
                        });

                });

              });


  

           
        </script>
    @endpush
