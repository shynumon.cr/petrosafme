@extends('layouts.main')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="row cm-content-section m-0">
        <div class="col-12 page-title-main">
            <ul>
                <li>
                    <h4 class="MyriadPro-Bold">Reset Users</h4>
                </li>
                <li class="float-right">Last Login: <span
                        class="text-blue bold">{{ date('F d, Y h:i a', strtotime(Auth()->user()->lastLoginTime)) }}</span>
                </li>
            </ul>
        </div>
        <!--page-title-main end-->
        <div class="col-12 cm-content-main">
            <div class="col-12 table-main">
                <div class="col-12 table-filter">
                    <ul>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>Search Keyword</p>
                                <input class="input-field" placeholder="Search Here" id="search" type="text">
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>Start Date</p>
                                <input class="form-control input-field calendar" placeholder="Start Date" id="start_date"
                                    type="text" data-select="datepicker">
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>End Date</p>
                                <input class="form-control input-field calendar" placeholder="End Date" id="end_date"
                                    type="text" data-select="datepicker">
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>Camps</p>
                                <select name="camp_id" id="camp_id" class="time-slot">
                                    <option value="">All</option>
                                    <option value="-1">None</option>
                                    @foreach ($camps as $camp)
                                        <option value="{{ $camp->id }}">{{ $camp->campName }} - {{ $camp->company }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p class="d-none d-md-block">&nbsp;</p>
                                <span class="CM filter-btn" onclick="search()"><i class="fa fa-search"
                                        aria-hidden="true"></i>Search</span>
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>Sort by</p>
                                <select name="sub_status" id="sub_status" class="time-slot" onchange="sortData()";>
                                    <option value="">Select from list</option>
                                    <option value="name">Name</option>
                                    <option value="userType">Type</option>
                                    <option value="created_at">Added Time</option>
                                </select>

                            </div>
                        </li>

                        <li>
                            <div class="cm-field-main m-0">
                                <p>Show By</p>
                                <select name="show_by" id="show_by">
                                    @foreach ([10, 25, 50, 100] as $it)
                                        <option {{ $it == 50 ? 'selected' : '' }} value="{{ $it }}">
                                            {{ $it }} entries</option>
                                    @endforeach
                                </select>
                            </div>
                        </li>
                    </ul>
                </div>
                <table id="coordinator_table" class="table table-sm table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>
                                Sl. No.
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                Username
                            </th>
                            <th>
                                Type
                            </th>
                            <th>
                                Added Time
                            </th>
                            <th class="action">
                                Login Status
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <!--cm-content-main end-->
        </div>
        <!--cm-content-section end-->
    @endsection
    @push('scripts')
        <script type="text/javascript" src="{{ URL::asset('js/jquery.validate.js') }}"></script>
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            function resetLoginStatus(me, id) {
                swal({
                        title: "Are you sure?",
                        text: "Once changed, user will logged out from current device!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((yes) => {
                        if (yes) {
                            $.ajax({
                                url: '{{ Url('reset-user-login') }}',
                                type: 'post',
                                data: {
                                    id: id
                                },
                                success: function(data) {
                                    if (data.success == true) {
                                        dataTable.draw();
                                        swal("Success!", data.message, "success");
                                    } else {
                                        $(me).prop('checked', true);
                                        swal("Error!", data.message, "error");
                                    }
                                },
                                error: function(data) {
                                    $(me).prop('checked', true);
                                    wal("Error!", data.statusText, "error");
                                }

                            })
                        } else {
                            $(me).prop('checked', true);
                        }
                    });
            }
            var dataTable = '';
            $(document).ready(function() {
                $("#start_date,#end_date").datepicker({
                    autoclose: true
                });
                dataTable = $('#coordinator_table').DataTable({
                    'bFilter': false,
                    'bLengthChange': false,
                    'pageLength': 50,
                    'processing': true,
                    'serverSide': true,
                    'bSort': false,
                    'serverMethod': 'post',
                    //"bDestroy": true,
                    'ajax': {
                        'url': '{{ Url('list-reset-users') }}',
                        'data': function(data) {
                            data.from_date = dmy($('#start_date').val());
                            data.to_date = dmy($('#end_date').val());
                            data.keywordsearch = $('#search').val();
                            data.sub_status = $('#sub_status').val();
                            data.camp_id = $('#camp_id').val();
                        },
                        "complete": function(json, type) {
                            var _resp = $.parseJSON(json.responseText);
                        }
                    },
                    'columns': [{
                            data: 'slno'
                        },
                        {
                            data: 'name'
                        },
                        {
                            data: 'username'
                        },
                        {
                            data: 'usertype'
                        },
                        {
                            data: 'addedtime'
                        },
                        {
                            data: 'action'
                        },
                    ]
                });
                $("#product_form").validate({
                    ignore: [],
                    rules: {
                        name: {
                            required: true
                        },
                        username: {
                            required: true
                        },
                        usertype: {
                            required: true
                        },
                        camp_name: {
                            required: true
                        },
                        role: {
                            required: true
                        },
                    },
                    errorPlacement: function(err, ele) {
                        err.insertAfter(ele);
                    },
                    submitHandler: function(form, element) {
                        $(".submit_buttom").html('<i class="fa fa-spinner fa-spin"></i> Please Wait').prop(
                            'disabled', true);
                        fm = new FormData(form);
                        $.ajax({
                            url: '{{ Url('add-edit-user') }}',
                            type: 'post',
                            data: fm,
                            cache: false,
                            processData: false,
                            contentType: false,
                            success: function(data) {
                                if (data.status == 'success') {
                                    $("#product_form")[0].reset();
                                    $(".add-member-popup-wrapper").hide(500);
                                    dataTable.draw();
                                    swal("Success!", data.message, "success");
                                    $(".submit_buttom").html('Save').prop('disabled', false);

                                } else
                                    swal("Error!", data.message, "error");
                            }
                        })
                    }
                })
                $('#show_by').change(function() {
                    dataTable.page.len($(this).val()).draw();
                })
            });

            function search() {
                dataTable.draw();
            }

            function sortData() {
                dataTable.draw();
            }

            function dmy(a) {
                if (a == '')
                    return '';
                a = a.split('/');
                b = [];
                b.push(a[2]);
                b.push(a[0]);
                b.push(a[1]);
                b = b.join('-');
                return b;
            }
        </script>
        <style>
            .error {
                color: red;
                font-size: 13px;
                display: block;
                text-align: left;
            }
        </style>
    @endpush
