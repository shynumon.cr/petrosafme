
<!DOCTYPE html>
<html class="h-100" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Petrosafe Admin</title>
    <link rel="icon" type="image/png" sizes="16x16" href="https://www.petrosafeme.com/assets/img/favicon.png">
    
    <link href="css/style.css" rel="stylesheet">
    
</head>

<body class="h-100">
    
    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <div class="login-form-bg h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100">
                <div class="col-xl-6">
                    <div class="form-input-content">
                        <div class="card login-form mb-0">
                            <div class="card-body pt-5 text-center">
                                <a class="text-center" href="index.php">
								<img src="../utility/images/logo.png" alt="" style="width:150px;height:auto;margin:0px auto;">
								</a>
								
                               <!--  <form class="mt-5 mb-5 login-input" method="POST">
                                     -->
                                    <div class="form-group row-cols-6">
                                    <label class='pr-3'>
                                        <input  type="radio" id="old_web" name="sel_website" value="old_web" checked> Go to Old Site
                                    </label>
                                    <label>
                                        <input type="radio" id="new_web" name="sel_website" value="new_web"> Go to New Site
                                    </label>
                                </div>
                                    <button type="button" name="login_submit" class="btn login-form__btn submit w-100" value="1"  onClick="myFunction()">Confirm</button>
                               <!--  </form> -->
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  
    <script>
    $('#old_web').click(function() {
        $('#new_web').prop('checked', false); 
    });

    $('#new_web').click(function() {
        $('#old_web').prop('checked', false); 
    });


</script>
    

    <!--**********************************
        Scripts
    ***********************************-->
    <script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>
    <script>
     function myFunction(){
        const val = $('input[name=sel_website]:checked').val();
        if(val=="old_web"){
           window.location.replace("login.php");
        }else if(val=="new_web"){
            window.location.replace("laravel/login");
        }
    }
    </script>
</body>
</html>





