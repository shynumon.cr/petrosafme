<?php
date_default_timezone_set("Asia/Dubai");
include('connection.php');
session_start();
// get these from secure back-end sources
//$outlet = "d4515e78-da84-41a2-b9c9-4a0e2e9c8876";
$outlet = "2c36ce58-2c88-4aed-9b29-acfb53bde3c9";
//$apikey = "NjMyMzA3NzQtNWU5ZC00MzkwLTk4MzctYTY5ZjljNDcwMDAwOjk5YmU1OWQ3LTQ0Y2UtNDc3NC1hNmNjLTE2NmEyMjcxM2IxNg==";
$apikey = "MmQxZmUyNjMtNmRmMC00MGNkLTkyZmUtZWQ1ODBiOGM4ODRlOjM2NmE2NGQxLTkzMGYtNDJhMC1iOWFhLTg5OGNhMWE0ZWNhYg==";

$amount = '';
	
if (isset($_POST['sessionId'])) {
	$session = $_POST['sessionId'];
	$amount = $_POST['amount'];
	$f_name = $_POST['f_name'];
	$l_name = $_POST['l_name'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];
	$addr = $_POST['addr'];
	$custid=$_POST['cust_id'];
	$building=$_POST['building'];
	$flat=$_POST['flat'];
	
	$_SESSION['custid']=$custid;
	$in_amt=($amount/100);
	$curdatetime=date("Y-m-d H:i:s");
	
	$sql_first = "INSERT INTO payment (pay_cust_id,pay_cust_name,pay_building,pay_flat,pay_status,pay_status_text,pay_order_ref,pay_reference,paid_amount,pay_init_time,pay_completed_time)VALUES ('".$custid."','".$f_name."','".$building."','".$flat."','0','','','','".$in_amt."','".$curdatetime."','".$curdatetime."')";
	$resultImg_first = mysqli_query($con,$sql_first);
	$paymntid= mysqli_insert_id($con);
	
	
	try {
		$idData = identify($apikey);
		if (isset($idData->access_token)) {
			$token = $idData->access_token;
			$payData = pay($session, $token, $outlet, $amount, $f_name, $l_name, $email, $addr, $custid, $phone, $paymntid);
			//DB insert
			// $curdatetime=date("Y-m-d H:i:s");
			// $sql = "INSERT INTO payment (pay_cust_id,pay_cust_name,pay_status,pay_init_time)VALUES ('".$custid."','".$f_name."','0','".$curdatetime."')";
			// $resultImg = mysqli_query($con,$sql);
			// $payrecord_id= mysqli_insert_id($con);
			// $_SESSION['payrecord_id']=$payrecord_id;
			
			//DB ends
			echo(json_encode($payData));
			//echo $payData;
			// if($_SERVER['REMOTE_ADDR']=='202.88.237.77'){
			// mail('renjith@pi-digi.com','AD LPG online_payment_ajax.php', json_encode($payData));
			// }
			exit();
		}
	} catch (Exception $e) {
		echo($e->getMessage());
	}
}

/////////////////////
function identify($apikey) {
	//$idUrl = "https://identity-uat.ngenius-payments.com/auth/realms/ni/protocol/openid-connect/token";
	//$idUrl = "https://identity.ngenius-payments.com/auth/realms/ni/protocol/openid-connect/token";Live
	$idUrl = "https://identity.ngenius-payments.com/auth/realms/networkinternational/protocol/openid-connect/token";
	$idHead = array("Authorization: Basic ".$apikey, "Content-Type: application/x-www-form-urlencoded");
	$idPost = http_build_query(array('grant_type' => 'client_credentials'));
	$idOutput = invokeCurlRequest("POST", $idUrl, $idHead, $idPost, true);
	return $idOutput;
}

function pay($session, $token, $outlet, $amount, $f_name, $l_name, $email, $addr, $custid, $phone, $paymntid) {
	
	$refnce = ($paymntid + 1000);
	// construct order object JSON
	$ord = new stdClass;
	$ord->action = "SALE";
	$ord->amount = new stdClass;
	$ord->amount->currencyCode = "AED";
	$ord->amount->value = $amount;
	//$ord->merchantOrderReference ="PS-".date('Y')."-".mt_rand(10000,99999);
	$ord->merchantOrderReference ="PS-".date('Y')."-".$refnce;
	$ord->emailAddress =$email;
	$ord->billingAddress = new StdClass();
	$ord->billingAddress->firstName = $f_name;
	$ord->billingAddress->lastName = $l_name;
	$ord->billingAddress->address1 = $addr;
	$ord->billingAddress->city = 'Dubai';
	$ord->billingAddress->countryCode = 'UAE';
	$ord->merchantDefinedData = new StdClass();
	$ord->merchantDefinedData->custId = $custid;
	$ord->merchantDefinedData->mobile = $phone;
	$ord->merchantDefinedData->paymentid = $paymntid;
	
	//$payUrl = "https://api-gateway-uat.ngenius-payments.com/transactions/outlets/".$outlet."/payment/hosted-session/".$session;
	$payUrl = "https://api-gateway.ngenius-payments.com/transactions/outlets/".$outlet."/payment/hosted-session/".$session;
	$payHead = array("Authorization: Bearer ".$token, "Content-Type: application/vnd.ni-payment.v2+json", "Accept: application/vnd.ni-payment.v2+json");
	$payPost = json_encode($ord);
	
	$payOutput = invokeCurlRequest("POST", $payUrl, $payHead, $payPost, true);
	
	// print_r($payOutput);
	// exit();
	
	return $payOutput;
}

function invokeCurlRequest($type, $url, $headers, $post, $json) {
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
	
	if ($type == "POST" || $type == "PUT") {
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		if ($type == "PUT") {
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		}
	}
	
	$server_output = curl_exec ($ch);
	return json_decode($server_output);
}
?>