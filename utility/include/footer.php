

<div class="row clearfix principles" style=" overflow: hidden; margin: 0px !important; background: #fafafa;">
    <div class="auto-container">
    <div class="col-md-12 col-sm-12 mob-app-client-scroll-tmb-box no-left-right-padding">
          <div class="sec-title-one" style="margin-bottom: 0px !important;">
			<h2>Our Principles</h2>
		</div>
		  <div id="owl-demo-one" class="owl-carousel">
          
              <div class="item"><img src="images/n-client1.jpg" alt=""></div><!--item end-->
              <div class="item"><img src="images/n-client2.jpg" alt=""></div><!--item end-->
              <div class="item"><img src="images/n-client3.jpg" alt=""></div><!--item end-->
              <div class="item"><img src="images/n-client4.jpg" alt=""></div><!--item end-->
              <div class="item"><img src="images/n-client5.jpg" alt=""></div><!--item end-->
              <div class="item"><img src="images/n-client6.jpg" alt=""></div><!--item end-->
              <div class="item"><img src="images/n-client7.jpg" alt=""></div><!--item end-->
              <div class="item"><img src="images/n-client8.jpg" alt=""></div><!--item end-->
              <div class="item"><img src="images/n-client9.jpg" alt=""></div><!--item end-->
              
              
          </div><!--mob-app-client end-->
    </div>
    </div>
</div>




<footer class="main-footer">
    	
        <!--Footer Upper-->        
        <div class="footer-upper">
            <div class="auto-container">
                <div class="row clearfix">
                    
                    <!--Two 4th column-->
                    <div class="col-md-3 col-sm-12 col-xs-12">
                    	<div class="row clearfix">
                    		<!--Footer Column-->
                        	
                            
                            <!--Footer Column-->
                    		<div class="col-md-12 col-sm-12 col-xs-12 column">
                    			<div class="footer-widget gallery-widget">
                                    <div class="sec-title-three">
                                    	<h2>Contact Us</h2>
                                    </div>
                                    <div class="footer-widget logo-widget">
                                    <div class="logo"><a href="index.php"><img src="images/logo-2.png" class="img-responsive" alt=""></a></div>
                                    <div class="text">A member of HW Gas Group</div>
                                    
                                    <ul class="contact-info">
                                    	<li><span class="icon flaticon-pin"></span> PO Box: 3286, Dubai, UAE</li>
                                        <li><span class="icon flaticon-technology"></span> +971 4 2677 294</li>
                                        <li><span class="icon flaticon-mail-2"></span>utility@petrosafe.ae</li>
                                    </ul>
                                
                                </div>
                                </div>
                    		</div>
                    
                    	</div>
                    </div><!--Two 4th column End-->
                    
                    
                    <div class="col-md-3 col-sm-12 col-xs-12">
                    	<div class="row clearfix">
                    		<!--Footer Column-->
                        	
                            
                            <!--Footer Column-->
                    		<div class="col-md-12 col-sm-12 col-xs-12 column">
                    			<div class="footer-widget gallery-widget">
                                    <div class="footer-widget links-widget">
                                    <div class="sec-title-three">
                                    	<h2>Quick Links</h2>
                                    </div>
                                    <ul>
                                        <li><a href="index.php">Home</a></li>
                                        <li><a href="about.php">About Us</a></li>
                                        <li><a href="services.php">Services</a></li>
                                        <li><a href="media.php">Media</a></li>
                                        <li><a href="projects.php">Projects</a></li>
                                        <li><a href="contact.php">Contact Us</a></li>
                                    </ul>
        
                                </div>
                                </div>
                    		</div>
                    
                    	</div>
                    </div>
                    
                    
                    <div class="col-md-3 col-sm-12 col-xs-12">
                    	<div class="row clearfix">
                    		<!--Footer Column-->
                        	
                            
                            <!--Footer Column-->
                    		<div class="col-md-12 col-sm-12 col-xs-12 column">
                    			<div class="footer-widget gallery-widget">
                                    <div class="sec-title-three">
                                    	<h2>Certifications</h2>
                                    </div>
                                    <div class="clearfix">
                                        <div class="certifications">
                                            <ul>
                                                <li><img src="images/certificates1.jpg" alt=""></li>
                                                <li><img src="images/certificates2.jpg" alt=""></li>
                                                <li><img src="images/certificates3.jpg" alt=""></li>
                                                <li><img src="images/certificates4.jpg" alt=""></li>                  
                                                <div class="clear"></div>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                    		</div>
                    
                    	</div>
                    </div>
                    
                    




                    
                    <div class="col-md-3 col-sm-12 col-xs-12">
                    	<div class="row clearfix">
                    		<!--Footer Column-->
                        	
                            
                            <!--Footer Column-->
                    		<div class="col-md-12 col-sm-12 col-xs-12 column">
                    			<div class="footer-widget gallery-widget">
                                    <div class="sec-title-three">
                                    	<h2>Group of Companies</h2>
                                    </div>
                                    <div class="clearfix">
                                        <div class="client-set">
                                            <ul>
                                                <li><a href="http://www.happywaygas.com/" target="_blank"><img src="images/client1.jpg" alt=""></a></li>
                                                <li><a href="https://oxypro.ae/" target="_blank"><img src="images/client2.jpg" alt=""></a></li>
                                                <li><a href="http://www.specenergyme.com/" target="_blank"><img src="images/client3.jpg" alt=""></a></li>
                                                <li><a href="http://www.oxyprotech.com/" target="_blank"><img src="images/client4.jpg" alt=""></a></li>
                                                <div class="clear"></div>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                    		</div>
                    
                    	</div>
                    </div>
                    
                </div>
                
            </div>
        </div>
        
        <!--Footer Bottom-->
    	<div class="footer-bottom">
            <div class="auto-container">
            	<div class="row clearfix">
                    <!--Copyright-->
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="copyright text-center">Copyrights &copy; 2003 - 2020 Petrosafe. All Rights Reserved.</div>
                    </div>
                    
                    
                
                </div>
            </div>
        </div>
        
    </footer>
    
    
<script src="js/jquery.js"></script> 
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/jquery.fancybox-media.js"></script>
<script src="js/owl.js"></script>
<script src="js/wow.js"></script>
<script src="js/isotope.js"></script>
<script src="js/script.js"></script>



<script src="js/owl.carousel.js"></script>
<script>
$(document).ready(function() {
	
  var owl = $("#owl-demo-one");
	owl.owlCarousel({
	items : 6, //10 items above 1000px browser width
	itemsDesktop : [1000,6], //5 items between 1000px and 901px
	itemsDesktopSmall : [900,6], // 3 items betweem 900px and 601px
	itemsTablet: [600,3], //2 items between 600 and 0;
	itemsMobile : [320,2], // itemsMobile disabled - inherit from itemsTablet option
	autoPlay : true,
    stopOnHover : true,
  });
  
  $(".next").click(function(){
    owl.trigger('owl.next');
  });
  
  $(".prev").click(function(){
    owl.trigger('owl.prev');
  });
  
  
  
});
</script>


<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/lightview.js"></script>



