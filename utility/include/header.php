<link href="css/owl.carousel.css" rel="stylesheet">
<header class="main-header header-style-one">


        
    	<!--Header-Upper-->
        <div class="header-upper">
        	<div class="auto-container">
            	<div class="clearfix">
                	
                	<div class="pull-left logo-outer">
                    	<div class="logo"><a href="index.php"><img src="images/logo.png" alt="Petrosafe" title="Petrosafe"></a></div>
                    </div>
                    
                    <div class="pull-right upper-right hed-right clearfix">
                    	
                        
                        
                        <!--Info Box-->
                        <div class="upper-column info-box">
                        	<div class="icon-box"><span class="flaticon-technology"></span></div>
                            <ul>
                            	<li><strong>+971 4 267 7294</strong></li>
                                <li>info@petrosafeme.com</li>
                            </ul>
                        </div>
                        
                        <!--Info Box-->
                        <div class="upper-column info-box">
                        	<!--<div class="link-btn">
                            	<a href="#" class="theme-btn btn-style-one">get a quote</a>
                            </div>-->
                            
                            <div class="icon-box"><span class="flaticon-inbox"></span></div>
                            <ul>
                            	<li><strong>Downloads</strong></li>
                                <li>PDF Brochure</li>
                            </ul>
                            
                        </div>
                        
                        
                        <!--Info Box-->
                        <div class="upper-column info-box no-top-padding no-left-padding">
                        
                        	<a href="https://www.facebook.com/petro.safe.56" target="_blank"><div class="icon-box1"><span class="fa fa-facebook"></span></div></a>
                            <a href="https://twitter.com/petrosafeme" target="_blank"><div class="icon-box1"><span class="fa fa-twitter"></span></div></a>
                            <a href="https://www.linkedin.com/company/petrosafe-llc/" target="_blank"><div class="icon-box1"><span class="fa fa-linkedin"></span></div></a>
                            
                            
                            <div class="clear"></div>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
        
        <!--Header-Lower-->
        <div class="header-lower">
            
        	<div class="auto-container">
            	<div class="nav-outer clearfix">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->    	
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li class=""><a href="index.php">Home</a></li>       
                                <li><a href="about.php">About</a></li>
                                <li class="dropdown"><a href="#">Services </a> <ul>
                                                                                 <li><a href="services.php">Gas Systems</a></li>
                                                                                 <li><a href="fireservices.php">Fire Systems</a></li>
                                                                                 <li><a href="trading.php">Trading</a></li>
                                            
                                    </ul>
                                </li>
                                <li><a href="media.php">Media</a> </li><li class="dropdown"><a href="#">Projects </a> 
                                      <ul>
                                        <?php    
										      
										   $query = "SELECT * FROM category";
										   $result = $con->query($query);
										   foreach($result as $row){
												 ?>
                                         <li><a href="projects.php?id=<?php echo $row['id'];?>"><?php echo $row['name']; ?> </a>
                                          <?php } ?>
                                    </ul>
                                </li>
                                <li><a href="contact.php">Contact</a></li>
                            </ul>
                        </div>
                    </nav>
                    <!-- Main Menu End-->
                    <!--<div class="btn-outer enquiry">
                         <a href="contact.php" class="theme-btn quote-btn"><span class="fa fa-mail-reply-all"></span> <label>Post </label>Enquiry</a>
                    </div>-->
                    
                    
                    <div class="btn-outer enquiry" title="Click to Payment" style="display:none;">
                         <a href="#" class="theme-btn quote-btn"><span class="fa fa-credit-card-alt" style="transform: rotate(-30deg);"></span> Online Payment</a>
                    </div>
                    
                </div>
                
            </div>
        </div>
        
        <!--Sticky Header-->
        <div class="sticky-header">
        	<div class="auto-container clearfix">
            	<!--Logo-->
            	<div class="logo pull-left">
                	<a href="index.php" class="img-responsive"><img src="images/logo.png" alt="Petrosafe" title="Petrosafe"></a>
                </div>
                
                <!--Right Col-->
                <div class="right-col pull-right">
                	<!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->    	
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                 <li class=""><a href="index.php">Home</a></li>       
                                <li><a href="about.php">About</a></li>
                                <li class="dropdown"><a href="#">Services </a> <ul>
                                                                                 <li><a href="services.php">Gas Systems</a></li>
                                                                                 <li><a href="fireservices.php">Fire Systems</a></li>
                                                                                 <li><a href="trading.php">Trading</a></li>
                                            
                                    </ul>
                                <li ><a href="media.php">Media</a> </li><li class="dropdown"><a href="#">Projects </a> 
                                      <ul>
                                        <?php    
										      
										   $query = "SELECT * FROM category";
										   $result = $con->query($query);
										   foreach($result as $row){
												 ?>
                                         <li><a href="projects.php?id=<?php echo $row['id'];?>"><?php echo $row['name']; ?> </a>
                                          <?php } ?>
                                    </ul>
                                </li>
                                <li><a href="contact.php">Contact</a></li>
                                
                                
                                <li title="Click to Payment" style="display:none;">
                                      <div class="btn-outer enquiry" style="position:relative;">
                                           <a href="#" class="theme-btn quote-btn"><span class="fa fa-credit-card-alt" style="transform: rotate(-30deg);"></span> Online Payment</a>
                                      </div>
                                </li>
                                
                                
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
                
            </div>
        </div><!--End Sticky Header-->
    
    </header>