<?php
date_default_timezone_set("Asia/Dubai");
include('connection.php');
session_start();
$customer_id=$_GET['id'];
$output ='';
$amount = 0;
if(is_numeric($customer_id))
{
	//$url = 'https://erp.hwgas.ae/utility/customer_list';  
	$url = 'https://utility.happywaygas.com/utility/customer_list';  
	$data = array(
		'user_id' => '14',
		'customer_id' => $customer_id
	);
	$payload = json_encode(array("params" => $data));	
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
	$output = json_decode(curl_exec($ch));
	// echo '<pre>';
	// print_r($output);
	// exit();
	$errlat = curl_error($ch);
	
	curl_close($ch);
}
$show_card_view = '0';
if($_POST['hid_sub_btn'] == 1)
{
	$f_name=$_POST['hid_first_name'];
	$l_name=$_POST['hid_last_name'];
	$amount=$_POST['txt_out_amt'];
	if($_POST['hid_email'] == "")
	{
		$email="utility@hwgas.ae";
	} else {
		$email=$_POST['hid_email'];
	}
	$addr=$_POST['hid_billing_address'];
	$custid=$_POST['hid_custid'];
	
	$payAmount = $amount*100;
	$show_card_view = '1';
}
?>
<!doctype html>
<base href="https://petrosafeme.com/">
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="utility/images/em-favicon.png"/>
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="utility/css/bootstrap.css" >
    <link rel="stylesheet" type="text/css" href="utility/css/style.css"/>
    
    <link rel="stylesheet" type="text/css" href="utility/css/font-awesome.min.css"/>
    <!--<script src="https://paypage.sandbox.ngenius-payments.com/hosted-sessions/sdk.js"></script>-->
    <script src="https://paypage.ngenius-payments.com/hosted-sessions/sdk.js"></script>
    
    <title>Petrosafeme | Committed to gas and fire safety</title>
	<style>
	#mount-id{ height: 100% !important; }
	#3ds_iframe{ height: 100% !important; }
	</style>
</head>
<body>



<header class="nps-header">
   <div class="auto-container">
       <div class="row clearfix">
           <div class="col-md-12 col-sm-12 no-left-right-padding">
           
                <div class="col-lg-3 col-md-3 col-sm-12 pl-0 pr-0">
                     <div class="logo"><a href="utility/payment"><img src="utility/images/logo.png" alt=""></a></div>
                     <div class="mob-menu-icon"><img src="utility/images/menu.png"></div><!--logo end-->
                     <div class="clear"></div> 
                </div>
                
                <div class="col-lg-9 col-md-9 col-sm-9 nps-menu menu pl-0 pr-0">
                     <nav id="primary_nav_wrap">
                          <ul>                                   
                              
                              <li class="nps-icon2 active"><a href="utility/payment"> &nbsp;&nbsp; Online Payment</a></li>
                              <li class="nps-icon1"><a href="utility/self-reading">Self Reading</a></li>
                              <li class="nps-icon3"><a href="utility/register">Register</a></li>
                              
                              <div class="clear"></div>
                          </ul>
                          <div class="clear"></div>   
                     </nav>
                    <div class="clear"></div>
                </div>
           
           
           </div>
       </div>
   </div>
</header>




<section class="industry-section mr-set-rate nps-pb-100">
    <div class="auto-container">
      <div class="row clearfix">
      
           <h2 class="text-center">Online Payment
           
               
               <?php
				if($_GET['id'] != "")
				{
				?>
                            <span class="nps-back"><a href="https://petrosafeme.com/utility/payment"><i class="fa fa-chevron-left"></i> &nbsp; BACK</a></span>
							<?php
				}
				 ?> 
           
           
           </h2>
           
           <div class="col-md-12 col-sm-12  no-left-right-padding">
                
                
                
                
                
                
                
                
                <section class="contact-form-section">
		<div class="auto-container">
            <!-- Contact Form -->
            <div class="default-form contact-form" style="padding-top: 50px;">
				<?php
				if($_GET['id'] == "")
				{
				?>
				<div class="row clearfix" id="customeridbtn">
					<div id="msg"></div>
					<?php if($output->result->status=='failed'){?>
					<div class="alert alert-danger"><strong><?php echo $output->result->message;?></strong></div>
					<?php }?>
                    
                    <div class="col-md-4 col-sm-8 box-center cus-id-set no-left-right-padding">
                    
                         <div class="col-md-12 col-md-12 form-group no-left-right-padding">
                              <p class="mb-0"><strong>Customer ID</strong></p>
						      <input type="text" name="name" id="custid" value="<?php echo $customer_id;?>" placeholder="Enter Customer ID">
                         </div>
                         
                         <div class="col-md-12 col-md-12 no-left-right-padding">
						      <button type="button" class="theme-btn btn-style-three" id="submit" name="submit" style=" width: 100%;">Submit</button>
                         </div>
                         
                    </div>
                    
                    
                    
                    
                    
				 </div>
				 <?php
				}
				 ?>
				 
				 <div class="row clearfix" id="successdiv" style="display: none;">
				 <div id="successdivdetail" style="display: none;">
						<div class="success-set"><img src="utility/images/success.gif" alt=""></div>
                     <h2 class="text-center" style="line-height: 34px;"><strong>THANK YOU</strong><br>Your payment has been successful!</h2>
					</div> 
					 <h2 id="errordivdetail" class="text-center" style="line-height: 34px;display: none;">Your payment has been failed!</h2>
					 
				 </div>
				<div class="row clearfix">
					
                    
				<?php 
				//print_r(json_encode($output));
				if($output->result->status=='success'){?> 
                  <div class="form-group col-md-6 col-sm-10 col-xs-12 box-center">
                     
                       <div class="col-md-12 col-sm-12 col-xs-12 field-box-main pl-0 pr-0">
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-left pl-0">Name <span>:</span></div>
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-right pl-0 pr-0"><?php echo $output->result->customer_list[0]->name;?></div>
                       </div>
                       
                       
                       <div class="col-md-12 col-sm-12 col-xs-12 field-box-main pl-0 pr-0">
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-left pl-0">Contract ID <span>:</span></div>
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-right pl-0 pr-0"><?php echo $output->result->customer_list[0]->contract_ref;?></div>
                       </div>
                       
                       
                       <div class="col-md-12 col-sm-12 col-xs-12 field-box-main pl-0 pr-0">
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-left pl-0">Building name <span>:</span></div>
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-right pl-0 pr-0"><?php echo $output->result->customer_list[0]->building_name;?> </div>
                       </div>
                       
                       
                       <div class="col-md-12 col-sm-12 col-xs-12 field-box-main pl-0 pr-0">
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-left pl-0">Flat name <span>:</span></div>
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-right pl-0 pr-0"><?php echo $output->result->customer_list[0]->flat_name;?> </div>
                       </div>
                       
                       
                       <div class="col-md-12 col-sm-12 col-xs-12 field-box-main pl-0 pr-0">
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-left pl-0">Contract number <span>:</span></div>
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-right pl-0 pr-0"><?php echo $output->result->customer_list[0]->contract_ref;?> </div>
                       </div>
                       
                       
                       <div class="col-md-12 col-sm-12 col-xs-12 field-box-main pl-0 pr-0">
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-left pl-0">Last reading <span>:</span></div>
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-right pl-0 pr-0"><?php echo $output->result->customer_list[0]->previous_reading;?> </div>
                       </div>
                       
                       
                       <div class="col-md-12 col-sm-12 col-xs-12 field-box-main pl-0 pr-0">
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-left pl-0">Last reading amount <span>:</span></div>
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-right pl-0 pr-0"><?php echo $output->result->customer_list[0]->last_invoiced_amount;?></div>
                       </div>
					   
					   <div class="col-md-12 col-sm-12 col-xs-12 field-box-main pl-0 pr-0 payamountsection" style="display: none;">
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-left pl-0">Payment Reference <span>:</span></div>
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-right pl-0 pr-0" id="payment_ref"></div>
                       </div>
					   
					   <div class="col-md-12 col-sm-12 col-xs-12 field-box-main pl-0 pr-0 payamountsection" style="display: none;">
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-left pl-0">Paid Amount <span>:</span></div>
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-right pl-0 pr-0" id="paid_amount"></div>
                       </div>
                       
                    <form action="" method="POST" id="submit_payment_form" >   
                       <div class="col-md-12 col-sm-12 col-xs-12 field-box-main pl-0 pr-0">
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-left pl-0">Total Outstanding <span>:</span></div>
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-right pl-0 pr-0"><input type="text" name="txt_out_amt" id="txt_out_amt" value="<?php if($amount > 0){ echo $amount; } else { echo $output->result->customer_list[0]->pending_amount; } ?>" placeholder=""> </div>
							<input type="hidden" name="hid_custid" id="hid_custid" value="<?php echo $customer_id;?>">
							
							<input type="hidden" name="hid_first_name" id="hid_first_name" value="<?php echo $output->result->customer_list[0]->name;?>">
							<input type="hidden" name="hid_last_name" id="hid_last_name" value="<?php echo $output->result->customer_list[0]->name;?>">
							<input type="hidden" name="hid_building" id="hid_building" value="<?php echo $output->result->customer_list[0]->building_name;?>">
							<input type="hidden" name="hid_flat" id="hid_flat" value="<?php echo $output->result->customer_list[0]->flat_name;?>">
							
							<?php
							if($output->result->customer_list[0]->email == "")
							{
								$email="utility@hwgas.ae";
							} else {
								$n_email= explode(",",$output->result->customer_list[0]->email);
								$email = $n_email[0];
							}
							if($output->result->customer_list[0]->mobile == "")
							{
								$phone="";
							} else {
								$phone=$output->result->customer_list[0]->mobile;
							}
							?>
							
							
							<input type="hidden" name="hid_email" id="hid_email" value="<?php echo trim($email);?>">
							<input type="hidden" name="hid_phone" id="hid_phone" value="<?php echo trim($phone);?>">
							<?php $bil_addr=$output->result->customer_list[0]->building_name;
								  $bil_addr.=(strlen($output->result->customer_list[0]->flat_name)>2)?', '.$output->result->customer_list[0]->flat_name:'';
								  $bil_addr.=(strlen($output->result->customer_list[0]->street)>2)?', '.$output->result->customer_list[0]->street:'';
								  $bil_addr=str_replace(',', '', $bil_addr);//special characets will cause error
							?>
							<input type="hidden" name="hid_billing_address" id="hid_billing_address" value="<?php echo $bil_addr;?>">
                       </div>
                       <div class="col-md-12 col-sm-12 col-xs-12 field-box-main pl-0 pr-0"  id="paymnt_msg_btn" style="display: none;">
                            <!--<div class="col-md-6 col-sm-6 col-xs-6 field-box-left pl-0">&nbsp;</div>-->
                            <div class="col-md-12 col-sm-12 col-xs-12 field-box-right pl-0 pr-0" id="paymnt_msg">&nbsp;</div>
                       </div>
					   
                       <!--<?php// if($show_card_view == '0') { ?>-->
                       <div class="col-md-12 col-sm-12 col-xs-12 field-box-main pl-0 pr-0" id="submitsec">
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-left pl-0">&nbsp;</div>
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-right pl-0 pr-0">
							<input type="hidden" name="hid_sub_btn" value="1">
							<button type="button" onclick="clickdiv()" class="theme-btn btn-style-three" id="paysubmit" style="width: 100%;" name="submit">Pay Now</button>
							</div>
                       </div>
					   <!--<?php// } ?>--> 
                    </form>   
                    
                      
                       <div class="form-group col-md-12 col-sm-12 col-xs-12 pr-0 pl-0" id="paymountsec" style="display: none;">
                       
                       <h4 style="padding-bottom: 5px;"><strong>Enter Card Details</strong></h4>
						
					   <div class="col-md-12 col-sm-12 col-xs-12 field-box-main new-crd-det">
                       
                            <div class="loader" style="display: none;">
                                <p><i class="fa fa-exclamation-triangle"></i> &nbsp; Please wait. Do not press refresh or back button</p>
                                <div class="cssload-container">
                                     <div class="cssload-speeding-wheel"></div>
                                </div>
                                
                            </div>
                       
							<div id="mount-id"></div>
							<div id="3ds_iframe" style="display: none; height: 500px !important;"></div>
					   </div>
				</div>
				
				<div class="form-group col-md-12 col-sm-12 col-xs-12" id="paysec" style="display: none;">
					   <div class="col-md-12 col-sm-12 col-xs-12 field-box-main pl-0 pr-0">
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-left pl-0">&nbsp;</div>
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-right pl-0 pr-0">
							<button type="button" onclick="createSession()" class="theme-btn btn-style-three" id="retrysec" style="width: 100%;" name="paynow">Pay Now</button>
							</div>
                       </div>
					   
				  </div>
				  
				  <div class="form-group col-md-12 col-sm-12 col-xs-12" id="retrypaysec" style="display: none;">
					   <div class="col-md-12 col-sm-12 col-xs-12 field-box-main pl-0 pr-0">
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-left pl-0">&nbsp;</div>
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-right pl-0 pr-0">
							<button type="button" onclick="retrypaysec()" class="theme-btn btn-style-three" id="" style="width: 100%;" name="paynow">Retry Payment</button>
							</div>
                       </div>
					   
				  </div>
                       
                       
                  </div>
				  <!--<div class="form-group col-md-6 col-sm-4 col-xs-12" id="paymountsec" style="display: none;">
						
					   <div class="col-md-12 col-sm-12 col-xs-12 field-box-main pl-0 pr-0">
							<div id="mount-id" style="height: 250px;"></div>
							<div id="3ds_iframe"></div>
					   </div>
				</div>
				<div class="form-group col-md-6 col-sm-4 col-xs-12" id="paysec" style="display: none;">
					   <div class="col-md-12 col-sm-12 col-xs-12 field-box-main pl-0 pr-0">
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-left pl-0">&nbsp;</div>
                            <div class="col-md-6 col-sm-6 col-xs-6 field-box-right pl-0 pr-0">
							<button type="button" onclick="createSession()" class="theme-btn btn-style-three" id="" style="width: 100%;" name="paynow">Check out</button>
							</div>
                       </div>
					   
				  </div>-->
				  
				<?php } ?>   
             </div>
            </div>
            <!--End Contact Form -->
               
        </div>
    </section>
    
    
    
    
                 
           </div>
           
      </div>
    </div>
  </section>





<footer class="nps-footer">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="col-md-12 col-sm-12 no-left-right-padding">
            
              <div class="col-md-5 col-sm-12 nps-foot-left no-left-right-padding">
                <p><strong>Member of HappyWay Group</strong><br>Petrosafe ©<?php echo date("Y");?> All Rights Reserved.</p>
              </div>
                 
                 <div class="col-md-2 col-sm-12 nps-foot-mid no-left-right-padding"><img src="utility/images/happy-logo.png"></div>
              <div class="col-md-5 col-sm-12 nps-foot-right no-left-right-padding">
                      <p><strong>Phone : +971 4 267 7294<br>Email  : utility@petrosafe.ae</strong></p> 
              </div>
            
            </div>
        </div>
    </div>
</footer>



<script src="utility/js/jquery.js"></script> 
<script src="utility/js/bootstrap.min.js"></script>
<script src="utility/js/jquery.fancybox.pack.js"></script>
<script src="utility/js/jquery.fancybox-media.js"></script>
<script src="utility/js/owl.js"></script>
<script src="utility/js/validate.js"></script>
<script src="utility/js/wow.js"></script>
<script src="utility/js/script.js"></script>


<!--End Google Map APi-->
<script>
$( document ).ready(function() {
	
	
	$('.mob-menu-icon') .click(function(){
  $('.menu').toggle(1000);
  });
  
  
  $('.mob-menu-hide') .click(function(){
  $('.menu').hide(1000);
  });
	
	
    $("#submit").click(function() 
	{
		$("#msg").html('');
		var custid=$("#custid").val();
		
		if(custid!='')
		  {
			  //alert(custid);
			  
			  window.location = "https://petrosafeme.com/utility/payment/"+custid;
		  }
		else
		  {
			  $("#msg").html('<div class="alert alert-danger"><strong>Please enter customer id !</strong></div>');
		  }
	
	});
	
	// $("#submit_payment_form").submit(function (e) {
		
		// var formId = this.id;  
		// var payamt=$("#txt_out_amt").val();
		
		// if(payamt>0)
		  // {
			  // $("#paymnt_msg").html('');
			  
		  // }
		// else
		// {
			// e.preventDefault();
			// $("#paymnt_msg").html('<div class="alert alert-danger"><strong>Please check the payment amount !</strong></div>');
		// }
	// });
});
</script>
<script>
function retrypaysec()
{
	var cust_id = $('#hid_custid').val();
	window.location.href = 'https://petrosafeme.com/utility/payment/'+cust_id;
	// $('#paymountsec').show();
	// setTimeout(function() {
		// cardfunc();
	// }, 2000);
}

function clickdiv()
{
	var payAmount = $('#txt_out_amt').val();
	//alert(payAmount);
	if(payAmount > 0)
	{
		//createSession();
		$("#paymnt_msg_btn").hide();
		$("#paymnt_msg").html('');
		$('#paysubmit').hide();
		$("#submitsec").hide();
		$('#paymountsec,#paysec').show();
	} else {
		$("#paymnt_msg").html('<div class="alert alert-danger"><strong>Please check the payment amount !</strong></div>');
		$("#paymnt_msg_btn").show();
		$("#submitsec").show();
		
	}
}
$(document).ready(function() 
{
	cardfunc();
});

function cardfunc()
{
	const style = {
		main: {
			borderRadius:"0px",
			borderColor:"#F00",
			borderWidth:"0px",
			borderStyle: "solid"} /* the style for the main wrapper div around the card input*/,
		base: {
			borderStyle:"solid",
			borderColor:"#CCC",
			background:"#FFF",	
			borderWidth:"1px"} /* this is the default styles that comes with it */,
		input: {
			color:"#000000",
			borderStyle:"solid",
			borderColor:"#CCC",
			borderRadius:"5px",
			borderWidth:"1px",
			backgroundColor:"#FFF",					
			padding:"6px"} /* custom style options for the input fields 
		/,
		invalid: {} /* custom input invalid styles */
	};

	/* Method call to mount the card input on your website */
	"use strict";
	window.NI.mountCardInput('mount-id' /* the mount id*/, {
		style: style,
		// Style configuration you can pass to customize the UI
		//apiKey: "YjZhNWY4M2EtNTU3MC00MmM3LTljNzQtYTM1YzMzYjUwMTJiOjkwM2M2MTQwLTcyYmItNDk5Yi1iZjhjLWI5MzZkMTNiODQ4Nw==",
		apiKey: "ZjQ2NDg4NWItNjMyOC00ODgzLTk4MzUtMmRkMjhhNjhkZDRkOjIzZWQzYzhlLTU5YWYtNDhkZS04MzFlLTZiZGFlZGMyNDdlYQ==",
		// API Key for WEB SDK from the portal
		//outletRef: "d4515e78-da84-41a2-b9c9-4a0e2e9c8876",
		outletRef: "2c36ce58-2c88-4aed-9b29-acfb53bde3c9",
		// outlet reference from the portal
		onSuccess: function() {
			//alert("success");
			 console.log("success");
		},
		// Success callback if apiKey validation succeeds
		onFail: function() {
			//alert("failed");
			//console.log("failed");
			$("#paymnt_msg").html('<div class="alert alert-danger"><strong>Failed</strong></div>');
			$("#paymnt_msg_btn,#paysec").show();
		}, // Fail callback if apiKey validation fails
		onChangeValidStatus: function (_ref) {
			var isCVVValid = _ref.isCVVValid,
				isExpiryValid = _ref.isExpiryValid,
				isNameValid = _ref.isNameValid,
				isPanValid = _ref.isPanValid;
			console.log(isCVVValid, isExpiryValid, isNameValid, isPanValid);					 						
		}
	});
}

var sessionId;
function createSession() {
	//$('.new-crd-det .loader').show();
	$('#txt_out_amt').attr('readonly', true);
	//var amtt = $('#txt_out_amt').val();
	var payAmount = ($('#txt_out_amt').val() * 100).toFixed(0);
	//alert(payAmount);
	var f_name = $('#hid_first_name').val();
	var l_name = $('#hid_last_name').val();
	var email = $('#hid_email').val();
	var phone = $('#hid_phone').val();
	var addr = $('#hid_billing_address').val();
	var cust_id = $('#hid_custid').val();
	var building_name = $('#hid_building').val();
	var flat_name = $('#hid_flat').val();
	window.NI.generateSessionId().then(function (response) {
		sessionId = response.session_id;
		//console.log(sessionId);
		$("#mount-id").hide();
		$("#paysec").hide();
		$('.new-crd-det .loader').show();
		$("#paymnt_msg_btn,#paymountsec h4").hide();
		$("#paymnt_msg").html('');
		 $.ajax({
			 url: 'utility/online_payment.php',
			 type: 'post',
			 dataType: 'json',
			 // data: { sessionId: sessionId, amount:<?php echo($payAmount); ?>, f_name:'<?php echo $f_name; ?>', l_name:'<?php echo($l_name); ?>', email:'<?php echo($email); ?>', addr:'<?php echo($addr); ?>'},
			 data: { sessionId: sessionId,building:building_name,flat:flat_name, amount:payAmount, f_name:f_name, l_name:l_name, email:email, addr:addr, cust_id:cust_id, phone:phone },
			 cache   : false,
			 success: function (data) {
				$('.new-crd-det .loader').hide();
				$('#3ds_iframe').show();
				
				 console.log(data);
				 $('#mount-id,#paysec').hide();
				 //alert(data);
				 //var jsonData = JSON.parse(data);
				 "use strict";
				 window.NI.handlePaymentResponse(data, {
					 mountId: '3ds_iframe',
					 style: {
						 width: 500,
						 height: 800,
						 fontSize:'10px'
					 }
				 }).then(function (response) {
					
					 var status = response.status;
					 //console.log(response);
					 if (status === window.NI.paymentStates.AUTHORISED || status === window.NI.paymentStates.CAPTURED) {
						
						 window.location.href = 'utility/payment-process.php?ref='+data.orderReference+'&id='+cust_id;								
					 } else if (status === window.NI.paymentStates.FAILED || 
						status === window.NI.paymentStates.THREE_DS_FAILURE) {
						$('.new-crd-det .loader').hide();
						$("#mount-id,#3ds_iframe").html('');
						$('#paymountsec,#paysec').hide();
						$("#paymnt_msg").html('<div class="alert alert-danger"><strong>Payment Failed</strong></div>');
						$('#retrypaysec').show();
						$("#paymnt_msg_btn").show();
						cardfunc();
					 } else {
						 $('.new-crd-det .loader').hide();
						 $("#mount-id,#3ds_iframe").html('');
						 $('#paymountsec,#paysec').hide();
						$("#paymnt_msg").html('<div class="alert alert-danger"><strong>Payment Failed</strong></div>');
						$('#retrypaysec').show();
						$("#paymnt_msg_btn").show();
						cardfunc();
					 }
				 });
			 },
			 error:function()
			 {
				 $('.new-crd-det .loader').hide();
				 $("#mount-id,#3ds_iframe").html('');
				 $('#paymountsec,#paysec').hide();
				 $("#paymnt_msg").html('<div class="alert alert-danger"><strong>Payment Failed</strong></div>');
				 $('#retrypaysec').show();
				 $("#paymnt_msg_btn").show();
				 cardfunc();
			 } 
		 });
	}).catch(function (error) {
		//alert(error);
		//return console.error(error);
		//$("#mount-id,#3ds_iframe").html('');
		//$('#paymountsec').hide();
		$("#paymnt_msg").html('<div class="alert alert-danger"><strong>'+error+'</strong></div>');
		//$("#retrysec").html('Retry Payment');
		$("#paymnt_msg_btn,#paysec").show();
		//cardfunc();
	});
}
  </script>
  

            



  </body>
</html>