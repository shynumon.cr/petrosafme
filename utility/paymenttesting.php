<?php
$url = 'https://utility.happywaygas.com/utility/payments';  
$data = array(
	'user_id' => 14,
	'customer_id' => 1891,
	'payment_method' => 9,
	'notes' => 'Payment reference number 4fb4d109-b608-4a52-a8eb-e5701a1b631c with order id PS-2020-59842',
	'amount' => 141.75,
	'date' => date('Y-m-d'),
);
$payload = json_encode(array("params" => $data));	

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
$pay_update_op = json_decode(curl_exec($ch)); 
echo '<pre>';
print_r($pay_update_op);
$errlat = curl_error($ch);

curl_close($ch);

?>