<?php include('connection.php'); 
$customer_id=$_GET['id'];
date_default_timezone_set("Asia/Dubai");
 
$usr_insrt_read=0; 
  if(is_numeric($customer_id))
  {
	$url = 'https://utility.happywaygas.com/utility/customer_list';  
	$data = array(
		'user_id' => '14',
		'customer_id' => (float)$customer_id
	);
	$payload = json_encode(array("params" => $data));	
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
	$output = json_decode(curl_exec($ch)); 
	//$access_token = $output->access_token;
	$errlat = curl_error($ch);
	
	curl_close($ch);
	// echo '<pre>';
    // print_r($output);exit();
	
	$today_date=date("Y-m-d");
	$query = "SELECT * FROM meter_reading_updates WHERE DATE(upd_added_datetime)='".$today_date."' AND upd_cust_id='".$customer_id."'";
	$result = mysqli_query($con, $query);
	$usr_insrt_read=mysqli_num_rows($result);
  }
  $insertmsg=0;
  if(isset($_POST['reading_submit']))
  {
	  $reading=$_POST['txt_reading'];
	  $cust_id=$_POST['hid_custid'];
	  $cust_name=$_POST['hid_cust_name'];
	  $cust_email=$_POST['txt_email'];
	  $cust_buildname=$_POST['txt_build_name'];
	  $cust_flatname=$_POST['txt_flat_name'];
	  $cust_mob=$_POST['txt_mob'];
	  $previous_reading_date=$_POST['previous_reading_date'];
	  //echo $reading.",".$cust_id;
	  
	  $target_dir = "uploads/readings/";
	  $target_file = $_POST['hid_image_url'];
	  //move_uploaded_file($_FILES["meter_image"]["tmp_name"], $target_file);
	  
	  $curdatetime=date("Y-m-d H:i:s");
	  //$sql = "INSERT INTO meter_reading_updates (upd_cust_id,upd_cust_name,upd_reading,upd_image,upd_added_datetime)VALUES ('".$cust_id."','".$cust_name."','".$reading."','".$target_file."','".$curdatetime."')";
	  $sql = "INSERT INTO meter_reading_updates (upd_cust_id,upd_cust_name,upd_cust_email,upd_cust_mob,upd_cust_building_name,upd_cust_flat_name,previous_reading_date,upd_reading,upd_image,upd_added_datetime)VALUES ('".$cust_id."','".$cust_name."','".$cust_email."','".$cust_mob."','".$cust_buildname."','".$cust_flatname."','".$previous_reading_date."','".$reading."','".$target_file."','".$curdatetime."')";
	  $rslt = mysqli_query($con,$sql);
	  $insertmsg=1;
  }
	
?>
<!doctype html>
<base href="https://petrosafeme.com/utility/">
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="images/em-favicon.png"/>
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" >
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
	<?php if($output->result->customer_list[0]->spec==true){ ?>
    <title>Specenergy</title>
	<?php } else { ?>
	<title>Petrosafe</title>
	<?php } ?>
	<script src="js/exif.js"></script> 
</head>
<body>



<header class="nps-header">
   <div class="auto-container">
       <div class="row clearfix">
           <div class="col-md-12 col-sm-12 no-left-right-padding">
           
                <div class="col-lg-3 col-md-3 col-sm-12 pl-0 pr-0">
                     <div class="logo">
						<a href="payment">
							<?php if($output->result->customer_list[0]->spec==true){ ?>
							<img src="images/spec-logo.png" alt="">
							<?php } else { ?>
							<img src="images/logo.png" alt="">
							<?php } ?>
						</a>
					</div>
                     <div class="mob-menu-icon"><img src="images/menu.png"></div><!--logo end-->
                     <div class="clear"></div> 
                </div>
                
                <div class="col-lg-9 col-md-9 col-sm-9 nps-menu menu pl-0 pr-0">
                     <nav id="primary_nav_wrap">
                          <ul>                                   
                              
                              <li class="nps-icon2"><a href="payment"> &nbsp;&nbsp; Online Payment</a></li>
                              <li class="nps-icon1 active"><a href="self-reading">Self Reading</a></li>
                              <li class="nps-icon3"><a href="register.php">Register</a></li>
                              
                              <div class="clear"></div>
                          </ul>
                          <div class="clear"></div>   
                     </nav>
                    <div class="clear"></div>
                </div>
           
           
           </div>
       </div>
   </div>
</header>

<section class="industry-section mr-set-rate nps-pb-100">
    <div class="auto-container">
      <div class="row clearfix">
      
           <h2 class="text-center">Meter Reading</h2>
		   <?php
				if($_GET['id'] == "")
				{
				?>
				<div class="row clearfix" id="customeridbtn" style="margin-bottom: 15px;">
					<div id="msg"></div>
					<?php if($output->result->status=='failed'){?>
					<div class="alert alert-danger"><strong><?php echo $output->result->message;?></strong></div>
					<?php }?>
                    
                    <div class="col-md-4 col-sm-8 box-center cus-id-set no-left-right-padding mr-fld-main" style="margin-bottom: 0px;padding-bottom: 10px;padding-top: 10px;">
                    
                         <div class="col-md-12 col-md-12 form-group no-left-right-padding">
                              <p class="mb-0" style="margin-bottom: 7px !important;"><strong>Customer ID</strong></p>
						      <input type="text" name="name" id="custid" class="mr-fld" value="<?php echo $customer_id;?>" placeholder="Enter Customer ID">
                         </div>
                         
                         <div class="col-md-12 col-md-12 no-left-right-padding">
						      <button type="button" class="theme-btn btn-style-three" id="submit" name="submit" style=" width: 100%;">Submit</button>
                         </div>
                         
                    </div>
                    
                    
                    
                    
                    
				 </div>
				 <?php
				}
				 ?>
		   <?php if($insertmsg==1){ ?>
           <div class="alert alert-success text-center"><strong>Meter reading updated successfully</strong></div>
		   <?php }
				 if($usr_insrt_read!=0){
			?>
			<div class="alert alert-danger text-center"><strong>You have already submitted a reading today!</strong></div>
			<?php } ?>
           <div class="col-md-12 col-sm-12 mr-main-box no-left-right-padding">
                <div class="col-md-4 col-sm-4 mr-left-box no-left-right-padding">&nbsp;</div>
                <div class="col-md-8 col-sm-8 mr-right-box ">
                
                     <div class="col-md-12 col-sm-12 no-right-padding">
                          <h4>Submit your meter reading</h4>
                     </div>
                  <form action="https://petrosafeme.com/utility/reading-submit" method="POST"  enctype="multipart/form-data" id="reading_submit_form">
                     <div class="col-md-6 col-sm-6 mr-right-sub-box no-left-padding">
                     
                          
                          
                          
                          <div class="col-md-12 col-sm-12 mr-fld-main no-right-padding">
                              <p>Name</p>
                              <input type="text" name="txt_name" id="txt_name" value="<?php echo $output->result->customer_list[0]->name;?>" class="mr-fld" placeholder="Enter your Name" required>
                          </div>
                          
                          
                          <div class="col-md-12 col-sm-12 mr-fld-main no-right-padding">
                              <p>Mobile</p>
                              <input type="text" name="txt_mob" id="txt_mob" value="<?php echo $output->result->customer_list[0]->mobile;?>" class="mr-fld" placeholder="Enter your Mobile" >
                          </div>
                          
                          <div class="col-md-12 col-sm-12 mr-fld-main no-right-padding">
                              <p>Email</p>
							  <input type="email" name="txt_email" id="txt_email" value="<?php echo $output->result->customer_list[0]->email;?>" class="mr-fld" placeholder="Enter your Email" required>
                          </div>
						  
                          <div class="col-md-12 col-sm-12 mr-fld-main no-right-padding">
                              <p>Building Name</p>
							  <input type="text" name="txt_build_name" id="txt_build_name" value="<?php echo $output->result->customer_list[0]->building_name;?>" class="mr-fld" placeholder="Enter building name" required>
                          </div>
                          
                          
                          <div class="col-md-12 col-sm-12 mr-fld-main no-right-padding">
                              <p>Flat Name</p>
							  <input type="text" name="txt_flat_name" id="txt_flat_name" value="<?php echo $output->result->customer_list[0]->flat_name;?>" class="mr-fld" placeholder="Enter flat name" required>
                          </div>
                     
                     </div>
                     
                     
                     <div class="col-md-6 col-sm-6 mr-right-sub-box mr-rit-set no-left-padding"> 
                     
                          
                          
                          
                          
                          
                          
                       <div class="col-md-12 col-sm-12 mr-fld-main no-right-padding">
                              <p class="hidettxt-set">&nbsp;</p>
                              <div class="col-md-12 col-sm-12 mr-red-img no-left-right-padding" id="image_preview"><img src="images/met-red-ex.jpg"></div>
                       </div>
                       
                       
                       
                       
                       <div class="col-md-12 col-sm-12 mr-fld-main no-right-padding">
                            <button type="button" class="theme-btn btn-style-three" name="upload_img_btn" id="upload_img_btn"><i class="fa fa-camera"></i> &nbsp; Take Meter Reading Picture</button>
							<input type="hidden" name="hid_image_url" id="hid_image_url" required/>
							<div id="imgname"></div>
                       </div>
                          
                          <div class="col-md-12 col-sm-12 mr-fld-main no-right-padding">
                              <p>Meter ID</p>
							  <input type="text" name="txt_meter_id" id="txt_meter_id" value="<?php echo $output->result->customer_list[0]->meter_number;?>" class="mr-fld" placeholder="Enter meter id" required>
                          </div>
                          <div class="col-md-12 col-sm-12 mr-fld-main  no-right-padding">
                              <p>Reading Value</p>
                              <input type="text" name="txt_reading" id="txt_reading" value="" class="mr-fld" placeholder="Enter your Meter Reading" required>
							   <input type="hidden" name="hid_custid" value="<?php echo $customer_id;?>" required/>
							   <input type="hidden" name="hid_cust_name" value="<?php echo $output->result->customer_list[0]->name;?>" />
							   <input type="hidden" name="previous_reading_date" value="<?php echo $output->result->customer_list[0]->previous_reading_date;?>"/>
							   
                          </div>
                          
                          
                     </div>
                     
                     
                     
                    <?php if($output->result->status=='success'&&$usr_insrt_read==0){?>  
                     <div class="col-md-6 col-sm-12 mr-fld-main">
                          <p></p>
                          <button type="submit" class="theme-btn btn-style-three" value="1" name="reading_submit" form="reading_submit_form">SUBMIT</button>
                     </div>
					<?php } ?>  
                  </form> 
				  <form method="POST"  enctype="multipart/form-data" id="img_upload_form">
					<input type="file" name="meter_image" id="meter_image" class="hidden"/>
					<input type="hidden" name="hid_img_or" value="" id="hid_img_or"/>
				  </form>
                </div> 
           </div>
           
      </div>
    </div>
  </section>


<!--<section class="industry-section mr-set-rate nps-pb-100">
    <div class="auto-container">
      <div class="row clearfix">
      
           <h2 class="text-center">Meter Reading</h2>
           
           <div class="col-md-12 col-sm-12 mr-main-box no-left-right-padding">
                <div class="col-md-4 col-sm-4 mr-left-box no-left-right-padding">&nbsp;</div>
                <div class="col-md-8 col-sm-8 mr-right-box ">
                
                     <div class="col-md-12 col-sm-12 no-right-padding">
                          <h4>Submit your meter reading</h4>
                     </div>
                
                     <div class="col-md-6 col-sm-6 mr-right-sub-box no-left-padding">
                     
                          
                          
                          
                          <div class="col-md-12 col-sm-12 mr-fld-main no-right-padding">
                              <p>Name</p>
                              <div class="col-md-12 col-sm-12 mr-filled-fld">Tom Mathew</div>
                          </div>
                          
                          
                          <div class="col-md-12 col-sm-12 mr-fld-main no-right-padding">
                              <p>Mobile</p>
                              <div class="col-md-12 col-sm-12 mr-filled-fld">0987 654 321</div>
                          </div>
                          
                          
                          <div class="col-md-12 col-sm-12 mr-fld-main no-right-padding">
                              <p>Building Number</p>
                              <div class="col-md-12 col-sm-12 mr-filled-fld">Skyline B26</div>
                          </div>
                          
                          
                          <div class="col-md-12 col-sm-12 mr-fld-main no-right-padding">
                              <p>Flat Number</p>
                              <div class="col-md-12 col-sm-12 mr-filled-fld">101 - B</div>
                          </div>
                     
                     </div>
                     
                     
                     <div class="col-md-6 col-sm-6 mr-right-sub-box mr-rit-set no-left-padding"> 
                     
                          
                          
                          
                          
                          
                          
                       <div class="col-md-12 col-sm-12 mr-fld-main no-right-padding">
                              <p class="hidettxt-set">&nbsp;</p>
                              <div class="col-md-12 col-sm-12 mr-red-img no-left-right-padding"><img src="images/met-red-ex.jpg"></div>
                       </div>
                       
                       
                       
                       
                       <div class="col-md-12 col-sm-12 mr-fld-main no-right-padding">
                            <button type="submit" class="theme-btn btn-style-three" name="submit"><i class="fa fa-camera"></i> &nbsp; Take Meter Reading Picture</button>
                       </div>
                          
                          
                          <div class="col-md-12 col-sm-12 mr-fld-main  no-right-padding">
                              <p>Reading Number</p>
                              <input type="text" name="name" id="name" value="" class="mr-fld" placeholder="Enter your Meter Reading">
                          </div>
                          
                          
                     </div>
                     
                     
                     
                     <div class="col-md-12 col-sm-12 mr-fld-main no-left-right-padding">
                     <div class="col-md-6 col-sm-12 mr-fld-main">
                          <p></p>
                          <button type="submit" class="theme-btn btn-style-three" name="submit">SUBMIT</button>
                       </div>
                     </div>
                </div> 
           </div>
           
      </div>
    </div>
  </section>-->




<footer class="nps-footer">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="col-md-12 col-sm-12 no-left-right-padding">
				<?php if($output->result->customer_list[0]->spec==true){ ?>
              <div class="col-md-5 col-sm-12 nps-foot-left no-left-right-padding">
                <p><strong>Office-1, MW-4, Plot 64, Mussafah</strong><br><strong>PO Box 9216, Abu Dhabi, UAE</strong></p>
              </div>
                 
			 <div class="col-md-2 col-sm-12 nps-foot-mid no-left-right-padding"><img src="images/spec-logo.png"></div>
              <div class="col-md-5 col-sm-12 nps-foot-right no-left-right-padding">
                      <p><strong>Phone : +971 2 443 9084<br>Email  : contact@specenergyme.com</strong></p> 
              </div>
				<?php } else { ?>
				<div class="col-md-5 col-sm-12 nps-foot-left no-left-right-padding">
                <p><strong>Member of HappyWay Group</strong><br>Petrosafe © 2020 All Rights Reserved.</p>
              </div>
                 
			 <div class="col-md-2 col-sm-12 nps-foot-mid no-left-right-padding"><img src="images/happy-logo.png"></div>
              <div class="col-md-5 col-sm-12 nps-foot-right no-left-right-padding">
                      <p><strong>Phone : +971 4 267 7294<br>Email  : utility@petrosafe.ae</strong></p> 
              </div>
				<?php } ?>
            
            </div>
        </div>
    </div>
</footer>

<script src="js/jquery.js"></script> 
<script>
$(document).ready(function() {
  
  $('.mob-menu-icon') .click(function(){
  $('.menu').toggle(1000);
  });
  
  
  $('.mob-menu-hide') .click(function(){
  $('.menu').hide(1000);
  });
  
  $("#submit").click(function() 
	{
		$("#msg").html('');
		var custid=$("#custid").val();
		
		if(custid!='')
		  {
			  //alert(custid);
			  
			  window.location = "https://petrosafeme.com/utility/self-reading/"+custid;
		  }
		else
		  {
			  $("#msg").html('<div class="alert alert-danger"><strong>Please enter customer id !</strong></div>');
		  }
	
	});
  
});
</script>

<script> 
 $( document ).ready(function() {
    $("#upload_img_btn").click(function() 
	{
		$("#meter_image").click();
	});
	
  /*$("#meter_image").change(function() 
	{*/
  document.getElementById('meter_image').onchange = function (e) {
	    $('#hid_img_or').val('');
		var filename = $('#meter_image')[0].files[0].name;
		$("#imgname").text(filename);
		
		const file = $('#meter_image')[0].files[0];
		const  fileType = file['type'];
			  //console.log(e.target.files);
		const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
		if (!validImageTypes.includes(fileType)) 
		 {
			 
			 $("#imgname").html('<div class="alert alert-danger"><strong>Only gif, jpeg & png extensions are allowed!</strong></div>');	
		 }
		else
		{
			
			getOrientation(document.getElementById('meter_image').files[0], function(orientation) {
				//alert('orientation: ' + orientation);
				$('#hid_img_or').val(orientation);
				
				
				
				$("#imgname").html('<div class="alert alert-warning"><strong>Uploading image, Please wait !!<i class="fa fa-spinner fa-spin" style="font-size:24px"></i></strong></div>');
				var form = $('#img_upload_form')[0];
				
				$.ajax({
						url: "upload_image.php",
						type: "POST",
						data:  new FormData(form),
						contentType: false,
						cache: false,
						processData:false,
						success: function(response)
								{
									var data = JSON.parse(response);
									//alert(data.files);
									if(data.status=='success')
									{
										$("#hid_image_url").val(data.image_url);
										$("#image_preview").html('<img src="'+data.image_url+'" />');
										$("#upload_img_btn").html('<i class="fa fa-camera"></i> &nbsp; Retake Meter Reading Picture');
										$("#imgname").html('');
									}
									else
									{
										$("#imgname").html('<div class="alert alert-danger"><strong>'+data.message+'</strong></div>');
									}
									//$("#imgname").html(data);
								},
								error: function() 
								{
									$("#imgname").html('<div class="alert alert-danger"><strong>Upload error.Try Again !</strong></div>');	
								} 	        
					  });
			});	
			
			
			
		}
			 
	};	
	
	
	$("#reading_submit_form").submit(function (e) {
		
		var formId = this.id;  
		var filenames=$('#meter_image').val();
		var reading=$('#txt_reading').val();
		
		if(filenames)
		  {
			  const file = $('#meter_image')[0].files[0];
			  const  fileType = file['type'];
			  
			  const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
			 if (!validImageTypes.includes(fileType)) 
			 {
				 e.preventDefault();
				 $("#imgname").html('<div class="alert alert-danger"><strong>Only gif, jpeg & png extensions are allowed!</strong></div>');	
			 }
			 else
			 {
				 var imgurl=$("#hid_image_url").val();
				 if(imgurl.length>2)
				 {
					 if(reading>0)
						{
							var filess = $('#meter_image')[0].files[0].name;
							$("#imgname").text(filess);
						}
					else
						{
							e.preventDefault();
							$("#imgname").html('<div class="alert alert-danger"><strong>Please check meter reading entered!</strong></div>');
						}
				 }
				 else
				 {
					 e.preventDefault();
					 $("#imgname").html('<div class="alert alert-danger"><strong>Please retry selecting image!</strong></div>');
				 }
				 
			 }
			  
			  
		  }
		else
		{
			e.preventDefault();
			$("#imgname").html('<div class="alert alert-danger"><strong>Please select meter reading image!</strong></div>');
		}
	});
	
	
});
function getOrientation(file, callback) { // function to check image orientation (some android devices had orientation change issue) 
    var reader = new FileReader();
    reader.onload = function(e) {

        var view = new DataView(e.target.result);
        if (view.getUint16(0, false) != 0xFFD8)
        {
            return callback(-2);
        }
        var length = view.byteLength, offset = 2;
        while (offset < length) 
        {
            if (view.getUint16(offset+2, false) <= 8) return callback(-1);
            var marker = view.getUint16(offset, false);
            offset += 2;
            if (marker == 0xFFE1) 
            {
                if (view.getUint32(offset += 2, false) != 0x45786966) 
                {
                    return callback(-1);
                }

                var little = view.getUint16(offset += 6, false) == 0x4949;
                offset += view.getUint32(offset + 4, little);
                var tags = view.getUint16(offset, little);
                offset += 2;
                for (var i = 0; i < tags; i++)
                {
                    if (view.getUint16(offset + (i * 12), little) == 0x0112)
                    {
                        return callback(view.getUint16(offset + (i * 12) + 8, little));
                    }
                }
            }
            else if ((marker & 0xFF00) != 0xFF00)
            {
                break;
            }
            else
            { 
                offset += view.getUint16(offset, false);
            }
        }
        return callback(-1);
    };
    reader.readAsArrayBuffer(file);
}
 </script>

  </body>
</html>