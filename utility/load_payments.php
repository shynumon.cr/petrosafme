<?php  
		session_start();
	    $adminid=$_SESSION['adminid'];	  
	    
	    include('connection.php');
		
		
		$readingsQuery = "SELECT * FROM meter_reading_updates ORDER BY upd_added_datetime DESC";
	
	    $resultReadings = mysqli_query($con,$readingsQuery);
		
		$i=1;
		
		
		
		
		
		
		
				## Read value
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$from_date=isset($_POST['Fromdate'])?$_POST['Fromdate']:'';
		$to_date=isset($_POST['Todate'])?$_POST['Todate']:'';
		$fromdate = str_replace('/', '-', $from_date);
		$frmdate = date('Y-m-d', strtotime($fromdate));
		$enddate = str_replace('/', '-', $to_date);
		$todate = date('Y-m-d', strtotime($enddate));
			
		$rowperpage = $_POST['length']; // Rows display per page
		$columnIndex = $_POST['order'][0]['column']; // Column index
		$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
		$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
		$searchValue = $_POST['search']['value']; // Search value

		## Search 
		$searchQuery = " ";
		if($searchValue != ''){
		   $searchQuery = " having paydate like'%".$searchValue."%' OR (pay_cust_name like '%".$searchValue."%' or 
				pay_reference like '%".$searchValue."%' or 
				pay_reference like '%".$searchValue."%' or 
				paid_amount like'%".$searchValue."%' ) ";
				
			$searchQuery2 = " and (pay_cust_name like '%".$searchValue."%' or 
				pay_reference like '%".$searchValue."%' or 
				pay_reference like '%".$searchValue."%' or 
				pay_building like '%".$searchValue."%' or 
				pay_flat like '%".$searchValue."%' or 
				paid_amount like'%".$searchValue."%' ) ";
		}
		
		$date_qry="";
		
		if($frmdate!='1970-01-01'&&$todate=='1970-01-01')
	    {
		   //console.log('start date only');
		   
		   $date_qry=" and DATE(pay_init_time)>='".$frmdate."' ";
	    }
	    else if($frmdate=='1970-01-01'&&$todate!='1970-01-01')
	    {
		   
		   $date_qry=" and DATE(pay_init_time)<='".$todate."' ";
		   
	    }
	    else if($frmdate!='1970-01-01'&&$todate!='1970-01-01')
	    {
		    if(strtotime($frmdate) <= strtotime($todate)){
			   $date_qry=" and DATE(pay_init_time)>='".$frmdate."' and DATE(pay_init_time)<='".$todate."' ";
			}
			
	    }

		## Total number of records without filtering
		$sel = mysqli_query($con,"select count(*) as allcount from payment");
		$records = mysqli_fetch_assoc($sel);
		$totalRecords = $records['allcount'];

		## Total number of record with filtering
		$sel = mysqli_query($con,"select count(*) as allcount,DATE_FORMAT(pay_init_time, '%d/%m/%Y') as paydate from payment WHERE 1 ".$searchQuery2.$date_qry);
		$sel_q="select count(*) as allcount,DATE_FORMAT(pay_init_time, '%d/%m/%Y') as paydate from payment WHERE 1 ".$searchQuery2.$date_qry;
		$records = mysqli_fetch_assoc($sel);
		$totalRecordwithFilter = $records['allcount'];

		## Fetch records
		$empQuery = "select *,DATE_FORMAT(pay_init_time, '%d/%m/%Y') as paydate from payment pay_init_time WHERE 1 ".$searchQuery2.$date_qry."  ORDER BY pay_init_time DESC  limit ".$row.",".$rowperpage;
		$empRecords = mysqli_query($con, $empQuery);
		$data = array();
		$i=($row)+1;
		while ($row = mysqli_fetch_assoc($empRecords)) {
			
		   $stat_text=$row['pay_status_text'];
		   $stat_text=strtolower($stat_text);
		   if($stat_text=='captured'){$stat="SUCCESS";}
		   else if($stat_text=='failed'){$stat="FAILED";}
		   else if($stat_text==''||$stat_text==NULL){$stat="STARTED";}
		   $init_datetime=$row['pay_init_time'];
		   $fin_datetime=$row['pay_completed_time'];
		   $pay_date=date('d/m/Y',strtotime($init_datetime));
		   $init_time=date('g:i:s A',strtotime($init_datetime));
		   $fin_time=date('g:i:s A',strtotime($fin_datetime));
		   $paydatetime = date('d/m/Y g:i A',strtotime($fin_datetime));
		   $pay_order_ref = $row['pay_order_ref'];
		   
		   $data[] = array( 
			  "sl_no"=>$i,
			  "cust_name"=>$row['pay_cust_name'],
			  "amount"=>$row['paid_amount'],
			  "building"=>$row['pay_building'],
			  "flat"=>$row['pay_flat'],
			  "order"=>$pay_order_ref,
			  "reference"=>$row['pay_reference'],
			  "status"=>$stat,
			  "init_time"=>$paydatetime
		   );
		   $i++;
		}

		## Response
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $totalRecords,
		  "iTotalDisplayRecords" => $totalRecordwithFilter,
		  "aaData" => $data,
		  "todate" => $frmdate
		);

		echo json_encode($response);
	   
?>